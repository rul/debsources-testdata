-----BEGIN PGP SIGNED MESSAGE-----

Source: tcpquota
Version: 1.6.13-2
Binary: tcpquota
Maintainer: Turbo Fredriksson <turbo@debian.org>
Architecture: all
Standards-Version: 2.1.2.2
Files: 
 4e9506eb17dd7ba8fe070b959d544d0b 75773 tcpquota_1.6.13.orig.tar.gz
 723036700bfd29297b2c62b17a99c135 3744 tcpquota_1.6.13-2.diff.gz

-----BEGIN PGP SIGNATURE-----
Version: 2.6.2i

iQCVAwUBNTZfLRZ4ByN4jNGpAQFH0AP/bhljZ/CAnmw23rToRuwYUjIn8SYDIhuu
eZ1KJbQHddtaRZT2sBYbq+6nzhsw8UBtnX7iDujoBVNHkPnvTqu7iLcrojHoJG92
V+Vj4HF2cPgZt/v/1e15gphM0rBxkVBB+HLnhbKe1CYGNKF4Z2FkfYVhT/H2kWut
mhOEnqgZHUo=
=3v7/
-----END PGP SIGNATURE-----
