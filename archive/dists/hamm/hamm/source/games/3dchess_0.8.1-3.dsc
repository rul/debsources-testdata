-----BEGIN PGP SIGNED MESSAGE-----

Source: 3dchess
Version: 0.8.1-3
Binary: 3dchess
Maintainer: Robert S. Edmonds <edmonds@freewwweb.com>
Architecture: any
Standards-Version: 2.4.0.0
Files: 
 5390c60953446e541d9455d9c4e38ca1 46371 3dchess_0.8.1.orig.tar.gz
 0306c4c6829a241694b9a786e0494d08 2743 3dchess_0.8.1-3.diff.gz

-----BEGIN PGP SIGNATURE-----
Version: 2.6.3a
Charset: noconv

iQEVAwUBNSBGu3oF6jxUeF+VAQFNOwgAiPVlfhJumSXu5RXLE/WBuj2vU+NyEz6l
q8Rly+y05OnDuqzj7+aVvb2BGO6DSM73Cv5n3V3GdqiNJ8C4cgeG+NPYPtGyfIEl
xmuORJgpkRGupWZOXdBMflwIHlrc1owQwl8SDxzR9J8rguNta1zU3+Z0OoFVCCv6
cjx8TaO3T/S01rkVymv1tDlShnOPBKVzDdKeQN6SMJXrwBGynrf8ZEujEd6H9DcW
VCFW1zM1P0FqXsELnAH8YBdBVtV14tyZRgPXO21H7cfEGSfUoT26+8whJ3Empt7V
6bHhXchcVjiu+S6+yELZDR+xW5xEoHk0szKOTN3y/REprQGND6p7NQ==
=DWuf
-----END PGP SIGNATURE-----
