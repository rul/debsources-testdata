-----BEGIN PGP SIGNED MESSAGE-----

Source: WMRack
Version: 1.0b3-1
Binary: wmrack
Maintainer: Marcelo E. Magallon <mmagallo@debian.org>
Architecture: any
Standards-Version: 2.4.0.0
Files: 
 a01368b35a71d37bfcf18fbe8368777b 63008 WMRack_1.0b3-1.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: 2.6.3ia
Charset: latin1

iQCVAwUBNTFVgC1VRe/SjlvlAQE5qwP+Jmpbcx0nInFFBkqEqi06DkCGwW/h+Xjq
f3TgTZPzxUQR36+bEi/yb7TBYumaJ/LN6V/8sBlockJXzWbVN1mvun6pQCdJNkwB
besQl6WN/Hj24/yaOR2nrzhxxbzBfELR3Aj8mdP2DV+lQ/0Po6EKCp6YGIA1W/Q6
FtwrzXZzZ+E=
=Zskp
-----END PGP SIGNATURE-----
