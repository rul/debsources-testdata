-----BEGIN PGP SIGNED MESSAGE-----

Source: adduser
Version: 2.11
Binary: adduser
Maintainer: Christoph Lameter <clameter@debian.org>
Architecture: any
Standards-Version: 2.1.0.0
Files: 
 08977b4504daa54b9e89902584d6b418 17465 adduser_2.11.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: 2.6.2

iQCVAwUBMknRYcDlhlk39XChAQFZcAQAg9+KuSOx/AvoK/5Q9+pogxcpDUYWAlb8
xqgNMHs3HXk+nAS20dWjo4xrjk1g6ktEXZ9/13dfeujEHHgHhOjyqjKG+adkKzpk
cjl50kCodqi0AvDIigNT3TMLC0LK/W4BmojWUfmxUyB5Whxv7+iI56VKEvhOKD7e
aD8aKHzkAkY=
=kFeS
-----END PGP SIGNATURE-----
