-----BEGIN PGP SIGNED MESSAGE-----

Source: bash
Version: 1.14.7-2
Binary: bash, libreadline2, libreadline2-dev
Maintainer: Guy Maor <maor@debian.org>
Architecture: any
Standards-Version: 2.1.1.0
Files: 
 929fc329d6eef97756f05056743911b3 1510188 bash_1.14.7.orig.tar.gz
 115e3ba6e4e18aeb334b1730609bd8c5 17300 bash_1.14.7-2.diff.gz

-----BEGIN PGP SIGNATURE-----
Version: 2.6.2

iQCVAwUBMpgyoPxmV0Di4lTBAQEAcgQAun4uhwqk0QnOK1NyJKQm8nzqWSaxX7o+
/naWHNL2oRgiRS7iNMFZohkZFWP+UHiJdT5/jZSG6WHvj6gGihHyzWlglRMmxAJS
hFyAdZf9Uv1pBSI9+6YZo8uranwSTetqLm+RO/wp9BM7Dk/yhc/+ImX2TabF1rLL
93YnUNTFvpk=
=jKZq
-----END PGP SIGNATURE-----
