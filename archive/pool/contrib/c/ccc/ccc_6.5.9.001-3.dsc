-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 1.0
Source: ccc
Version: 6.5.9.001-3
Binary: ccc
Maintainer: Adam C. Powell, IV <hazelsct@debian.org>
Architecture: alpha
Standards-Version: 3.6.1.1
Build-Depends: debhelper (>= 4.1.16)
Files: 
 f47a795032fa4f7739c9939e719ed449 8056 ccc_6.5.9.001-3.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.1 (GNU/Linux)

iD8DBQFCiQBRUm8B6FZO5LYRAmE1AJ4oShgyjJGgZ3MJYwVVU1DxTC3edwCeN2gZ
32Fw9BCJa8RzjwQW7YdjmyE=
=Xr1S
-----END PGP SIGNATURE-----
