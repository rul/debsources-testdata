-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 1.0
Source: zziplib
Version: 0.12.83-4
Binary: zziplib-bin, libzzip-0-12, libzzip-dev
Maintainer: Aurelien Jarno <aurel32@debian.org>
Architecture: any
Standards-Version: 3.6.1
Build-Depends: debhelper (>= 4.0.0), autotools-dev, pkg-config, zlib1g-dev
Files: 
 f7482fdb82fa9aed3f4dca55feb76ac1 710581 zziplib_0.12.83.orig.tar.gz
 9d850a9205955ec24851717bc6b71311 603167 zziplib_0.12.83-4.diff.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.2.5 (GNU/Linux)

iD8DBQFCFeL+w3ao2vG823MRArfgAJ90Uyoft5FHEpTAQYo2LnxvTpBntACaAq5X
Ey1K3mDXz4/Ed/mFhprkE6k=
=M/4W
-----END PGP SIGNATURE-----
