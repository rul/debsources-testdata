-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 1.0
Source: lha
Binary: lha
Architecture: any
Version: 1.14i-10.3
Maintainer: GOTO Masanori <gotom@debian.org>
Standards-Version: 3.6.0
Build-Depends: debhelper (>> 4.0.0)
Files: 
 10410742b0169f3357ef9a3f0f032037 64196 lha_1.14i.orig.tar.gz
 8e0050f9dabcabf085d69df1690d7dcc 45049 lha_1.14i-10.3.diff.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.6 (GNU/Linux)

iD8DBQFHUYAE7Ro5M7LPzdgRAuGjAJ4uXuHObXaHT5nhIQnW3BGZa8C+zwCg2Cs2
ooCbe7yFyzFSjy70iZKa86s=
=9sZ5
-----END PGP SIGNATURE-----
