-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: unrar-nonfree
Binary: unrar
Architecture: any
Version: 1:4.1.4-1
Maintainer: Martin Meredith <mez@debian.org>
Standards-Version: 3.9.2
Build-Depends: debhelper (>= 7)
Package-List: 
 unrar deb non-free/utils optional
Checksums-Sha1: 
 ae4b1e2c99e96527c4a97f980daa547499f42a0f 157135 unrar-nonfree_4.1.4.orig.tar.gz
 ab5e19e3aada8fb8762f470fad80a6d855a15240 5617 unrar-nonfree_4.1.4-1.debian.tar.gz
Checksums-Sha256: 
 705d93285fcd3b9c11f68e52d96395d942bf3c20172dcee14f0d78f1fee57361 157135 unrar-nonfree_4.1.4.orig.tar.gz
 2b676b423ffd90821f0378ca0be3828bf97d686f065e5d1fda88011dcf8c9b94 5617 unrar-nonfree_4.1.4-1.debian.tar.gz
Files: 
 808c92a661820a637ca1330cf40c18e4 157135 unrar-nonfree_4.1.4.orig.tar.gz
 dd94f01605d70f2e50f0f5769e73566f 5617 unrar-nonfree_4.1.4-1.debian.tar.gz
Autobuild: yes

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)

iQIcBAEBAgAGBQJPOuhXAAoJECr71n/RM6xuwpQP/jX50apVK18wqSdz4bKmJTFo
02F4PEtcP9XLFKkKqibnGI/6QGcDrLq3cu3wlObLHar+vancVso6krnDU9xIBJtz
I2lCEKzntPu8F2H7GYfy6R1GbcMXl+Hz15AnnQraNvNfp2ADwF9kS/AhuhiGQIX9
edz7gsV6mlzrItcivefG6/Ehn4F3iQc9bPM+Z2Jd5HxDkGnLdTpFaWs3YNlkaP6s
VHusg7z2oqRziBSwN4CA59LDhbNU/kXhLJ1M0OIv1rI/qZBru+FdwVh7/IZHfFCO
2EkMJrJmuSV110SCZxjfwu/zJp2nSKBI6qktV3mqV+4Amt2R+rbG15IUtvFxTiza
jlwkYHvOT9rNnSit9GixjrDsd87xEyehsNOisUdoPkwzq3+Qn6yUkt4kHYijGEIY
9ZdezTxr8OSpO3mpJm/R8vPLgcqeklJdnGEe3ahbqLwzy1yhFi6w4EwccapqAj2D
o0XqvruQp0METQE2ripGhmgw6TwNkk2ny1crhY7uXSydXRKM5hV8cfk4r4urw9V4
J/EdnJsw6GL0kwW2+PolHUm1bzWWp32d8MrZe80AftCPbk1QMLtrkcAjtysARUj7
TtSnAFJ2txS7rEi/WolKo9z+ZvpFLVrRfSnSpEafrbrmee2kF72PS7KslLssrptF
V4eP6PoUWMMG5k4mUMKO
=v7Jw
-----END PGP SIGNATURE-----
