-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: unrar-nonfree
Binary: unrar
Architecture: any
Version: 1:3.9.10-1
Maintainer: Martin Meredith <mez@debian.org>
Standards-Version: 3.8.4
Build-Depends: debhelper (>= 7)
Checksums-Sha1: 
 8654c4c539a0a05b8cf52ff1aa2fca180601c8e9 142718 unrar-nonfree_3.9.10.orig.tar.gz
 7c25ba8e21e00693c192f2956d290d5a2180a6bb 5456 unrar-nonfree_3.9.10-1.debian.tar.gz
Checksums-Sha256: 
 3b4627d8f5700588bc594afee6911865f55201625299f414438a654da488207a 142718 unrar-nonfree_3.9.10.orig.tar.gz
 b054f7a97c0ad5eb1a247330f9a9497eb1ea8fefba9051f743c549570b1a7e34 5456 unrar-nonfree_3.9.10-1.debian.tar.gz
Files: 
 3c130ae52ff9fece50af988c343e396d 142718 unrar-nonfree_3.9.10.orig.tar.gz
 b6300c74faf5c78e08309fb3d178374c 5456 unrar-nonfree_3.9.10-1.debian.tar.gz
Autobuild: yes

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)

iQIcBAEBAgAGBQJLrIwKAAoJECr71n/RM6xu2uwP/12FMnL9j+EnRFWV8i3lFhqk
auCt4qW/vQHcEzCWpMNobZhIU2Z+1TqtLnJw1eZEqLEmC0+Xu1+haOcT1+D98I8V
+HJO/gva8kKcivSDwBURuAJmVK9iP2wsBatCl3NgFG2wdFb20+lUv95iiY8xweho
iOgwgIxcBAbXP/2kBbVm4IRDZfQIHLglA5alYHabtJZ10hQG/+Va4+8wvNnWRb5v
WObPBaA/Z6YkaBV9QIISWGzi1WtiraVgRAYk9g8f7q/qTihWpjc7djDQgOZthU78
+K411BGvpNBNHf+im8EcAR9frxmVBOfvMYaltNhmKq14P5zmewky7dhL6wMbbp6H
a7nJ1PVsGkEgk2OS/1R7EJslCrlU1GSmVH3TikwPfwrgvjmDlGq+js2q8hzTk7YK
Rnd39joSPvR3r72TIQ2a8s+hkkQ/cnvThLM2Gm+0l8wb6RvdTLwT+kUjcnpgmSnK
GQApUa5nk5KNN837lYvceazLV0c90u0IAOYEUuH0b+6Srufqa5bZo8lyFfH9c7p8
+UvErfgEfZrcJnGFTnHNXJC4Pb2KNMrCovSyGCWjHimr+N2U4a06abgGSSVbBoeZ
jjPZoRv8STTy+mFAJ1K4FRrBFzCf2tu0t1w2sT9tS273HSNUVS3813TN2Bg07k24
O1Dt78TOo06hIUTymP7l
=WYmZ
-----END PGP SIGNATURE-----
