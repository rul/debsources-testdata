-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: make-doc-non-dfsg
Binary: make-doc
Architecture: all
Version: 4.0-2
Maintainer: Manoj Srivastava <srivasta@debian.org>
Homepage: http://www.gnu.org/software/make/
Standards-Version: 3.9.5
Vcs-Browser: http://git.debian.org/git/users/srivasta/debian/make-doc-non-dfsg.git
Vcs-Git: git://anonscm.debian.org/users/srivasta/debian/make-doc-non-dfsg.git
Build-Depends-Indep: texinfo, gettext, po-debconf, debhelper (>= 9.0.0), dh-autoreconf, autoconf, automake | automaken, cvs, autopoint, file, pkg-config, texlive-latex-base, texlive-fonts-recommended
Package-List: 
 make-doc deb non-free/doc optional arch=all
Checksums-Sha1: 
 b092020919f74d56118eafac2c202f25ff3b6e59 1872991 make-doc-non-dfsg_4.0.orig.tar.gz
 6b6708fbd54763d2a6f39dca9bad70b59627d7ec 22640 make-doc-non-dfsg_4.0-2.debian.tar.xz
Checksums-Sha256: 
 fc42139fb0d4b4291929788ebaf77e2a4de7eaca95e31f3634ef7d4932051f69 1872991 make-doc-non-dfsg_4.0.orig.tar.gz
 0686b10eeefce54953a0bab13d613f012d94d501e7eeb26cf6fc72bf49214d4d 22640 make-doc-non-dfsg_4.0-2.debian.tar.xz
Files: 
 b5e558f981326d9ca1bfdb841640721a 1872991 make-doc-non-dfsg_4.0.orig.tar.gz
 b4863af5709304c5804a86eb8f36452d 22640 make-doc-non-dfsg_4.0-2.debian.tar.xz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQFtBAEBCgBXBQJTa9RPUBSAAAAAABsALHNyaXZhc3RhQGdvbGRlbi1ncnlwaG9u
LmNvbUFCQTcxMDI1QTFCNUE4OEE0RTVGNjhDMjM2QkQ3MjBGNkY1NzY0NzJfNzA3
AAoJEDa9cg9vV2RyqrQH/iHDTHlWlfP38MvGzhwhYWlmYdjj7DhiLCUgBDD/MBgf
+dnFendvasuLZ9voMSfF/BTw2H3L31PjcbPSj5C4HJdx+NhOBQbQZyq8auyknr8z
5b84iKfuXlROeZDtErI9qHAsT5EG/kVV17yAdqz7tPXgf1tK2GRv6ZoUB4iGmKxN
0uQdaYA3G501fFcSOXnsKoFMdJYk6ihJv97m0LKPwzqnP69C8dd7mO5gJtO/7+BF
scoF59I6FBcxrzHN5vboDZmMRcFwYM8sHQWf4hoeRPSzUxxTKza6x/kMQuusXUSI
Rtjho55hLvdMXeIY1xpGIgaHsclttKsrb+Eb0PwLvk8=
=E2il
-----END PGP SIGNATURE-----
