-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 1.0
Source: bsdgames-nonfree
Binary: bsdgames-nonfree
Architecture: any
Version: 2.17-6
Maintainer: Aaron M. Ucko <ucko@debian.org>
Standards-Version: 3.9.4
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=users/ucko/bsdgames-nonfree.git
Vcs-Git: git://anonscm.debian.org/users/ucko/bsdgames-nonfree.git
Build-Depends: debhelper (>= 9), libncurses5-dev
Package-List: 
 bsdgames-nonfree deb non-free/games optional
Checksums-Sha1: 
 001d8d299643bd7654f277dc0f1437831690dd8b 181134 bsdgames-nonfree_2.17.orig.tar.gz
 6ffdf440049b3a3498700287c92fe33292911e63 8099 bsdgames-nonfree_2.17-6.diff.gz
Checksums-Sha256: 
 912a9253791ec9748dc49ab6cf7aacf417483c50ee04db57ed6d5dca110cb563 181134 bsdgames-nonfree_2.17.orig.tar.gz
 c559f12f3d706de84bd311c6e3220ced99cf6ff748f044c613637c076ea0e6cc 8099 bsdgames-nonfree_2.17-6.diff.gz
Files: 
 f70b5fe38c3dd82b44024263819621f9 181134 bsdgames-nonfree_2.17.orig.tar.gz
 167ee0a5911348da9f0c12e45a559395 8099 bsdgames-nonfree_2.17-6.diff.gz
Autobuild: yes

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.15 (GNU/Linux)

iQIcBAEBCAAGBQJSXVukAAoJEB5wkbHxSmSii/cP/10jdWgVx6MD1IHVRtM06roS
eDYw2misn3uv/ky4OZ310rPyw5iNoGbVNE1NTXkJWggcb6fcaF6mDgadX3HaOvAh
5gexgDs4ns0ejnSbwcqZoBmcfnjr0WJaiGyP9y1iVTNYZ/REPpLGH5BJtgIMuN4a
rTpqh47akrAqZKyMr4ZpUqg47og4BhMs29gt9NK8+P4B7R8nwPbb42l+yaQrdHW6
P2NbGWFLnzqeqUahET6y3wnywbcmghKQV3FPzKBoKYFiq621pA/8B/i/1bACKoAD
akd3nWErY5C2YY6/lTkhFL5OyabeKjXZM3x+YaPMJHEGAdIAzjnv2z66dhiSX4EA
h40bVvqy9OTtXQ6Ao3d7Suqq/pa9WpTU+qku1X0AqpfhN9xGoGQHaO5Q6WG2flmo
uhi/ofNwGEp+hQT8jeLDgbR8iN7/10qYvNI8QCpkmyVLMWXXkzvHPzjkV3KrOXWV
asZIzq3Cs3Et2+GdP2dNQlt4uSXXr3EkZPeoZXaiY0nFcjiUJ2ooZaZ+YGb1wfMR
RpuJdGeSPZAGVxJD3uDLBueBriZuUwZ8U+XMQ5wo5qhpEnpWtUx6TfFtgGK7ntCQ
kh3Vn5ufWT1DBVXakOlouZMsyXc1G47O7nGijvrlqgMMaI4tf0KrK1PiFKrcx/Bv
5hHeL74/cFcWqfFbpP3u
=Fklm
-----END PGP SIGNATURE-----
