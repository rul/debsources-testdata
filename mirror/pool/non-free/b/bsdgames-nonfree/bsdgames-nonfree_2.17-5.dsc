-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 1.0
Source: bsdgames-nonfree
Binary: bsdgames-nonfree
Architecture: any
Version: 2.17-5
Maintainer: Aaron M. Ucko <ucko@debian.org>
Standards-Version: 3.9.3
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=users/ucko/bsdgames-nonfree.git
Vcs-Git: git://anonscm.debian.org/users/ucko/bsdgames-nonfree.git
Build-Depends: debhelper (>= 9), libncurses5-dev
Package-List: 
 bsdgames-nonfree deb non-free/games optional
Checksums-Sha1: 
 001d8d299643bd7654f277dc0f1437831690dd8b 181134 bsdgames-nonfree_2.17.orig.tar.gz
 f9f3b5d4075cde4ff6f979fcd5375341fcef01a0 7867 bsdgames-nonfree_2.17-5.diff.gz
Checksums-Sha256: 
 912a9253791ec9748dc49ab6cf7aacf417483c50ee04db57ed6d5dca110cb563 181134 bsdgames-nonfree_2.17.orig.tar.gz
 092df0956db946d7009481e2c309c780e1945df9d88241649b16f108167510b1 7867 bsdgames-nonfree_2.17-5.diff.gz
Files: 
 f70b5fe38c3dd82b44024263819621f9 181134 bsdgames-nonfree_2.17.orig.tar.gz
 34036c04470125d5beba07abdf41d097 7867 bsdgames-nonfree_2.17-5.diff.gz
Autobuild: yes

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.12 (GNU/Linux)

iQIcBAEBCAAGBQJQwVpwAAoJEB5wkbHxSmSiPsgQAJzQAyuS9jDMZsX103FgIIAb
FlmZkuioEwaJeBbTAbDSU7T5bLYXxjYN5hbQPSPg7haCEytrZp+SW242TQZe9Ktf
mhSEOcJjoSVl8bKZuhJ3ADB2zd+XV3UyUNDBcXpEIcj2lWHbj+URhoS2GrJ723PM
P6lPOc4nJ62EYlmtKJ0rr9ODU3+PM1SgOCve1A3z7omM1K6yZaMT/RvzQ0FuCEjz
f2X7vU5TGmr8IQSRjbCDXTONXatTyI1cWHoPeKzHvQkj2+kWDm0L3jclrmhSDRM4
AXvXaOu7iIa45nqjsd8Xj6MVuC2bNiAh4+ryA4TTsH9OP2DvW070QVnf2xjfZMU2
tveYR6og7vrjz8StVDUQnUyLuMJuipKlFB9qDpGUYgXdnYxYSHUr/x3DwtiU+c/7
Gj5JzSwwYd0O0sruVY+fExg9tglwMNqnjmEydBn13GL/p0C3hHIRTss9xqqVWaNq
/nqxtUnfYB0USAiBDxPuIMS/YPaHAR5xOGDb+IWZAn/ej7x5aus4ifYyuaARZ9Pu
CKVdh3QWwmF1SRKzMCiA4kkGktyx5M/LzL3SEUYIoPKOnfDsKKyluv7dL6AR73ai
IG+KPpzeeqTYfO7NNVocv45b6NrVewTp5QjIcztgBpqmXwYv/UT9dXskuzrDW3Ku
vPt3B+E1ZevWgoElyBq0
=t3n4
-----END PGP SIGNATURE-----
