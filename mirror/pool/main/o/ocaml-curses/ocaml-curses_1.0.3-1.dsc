-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 1.0
Source: ocaml-curses
Binary: libcurses-ocaml, libcurses-ocaml-dev
Architecture: any
Version: 1.0.3-1
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders: Samuel Mimram <smimram@debian.org>, Sylvain Le Gall <gildor@debian.org>
Homepage: http://www.nongnu.org/ocaml-tmk/
Standards-Version: 3.8.3
Vcs-Browser: http://git.debian.org/?p=pkg-ocaml-maint/packages/ocaml-curses.git
Vcs-Git: git://git.debian.org/git/pkg-ocaml-maint/packages/ocaml-curses.git
Build-Depends: debhelper (>= 7), dpkg-dev (>= 1.13.19), libncurses5-dev, ocaml-nox (>= 3.11), dh-ocaml (>= 0.9.1), ocaml-findlib (>= 1.2.4), dpatch
Checksums-Sha1: 
 6bcb4a6eaf8353aac93069be0084f833a55340c1 54053 ocaml-curses_1.0.3.orig.tar.gz
 e9cdc5ad10f889371e821c3d22fbdeabed7f14c2 40076 ocaml-curses_1.0.3-1.diff.gz
Checksums-Sha256: 
 990a55ff99223edaa04387802907b00c475b46dd921dc5f8c5ede15ac673656f 54053 ocaml-curses_1.0.3.orig.tar.gz
 72bf6f6ed947ede40339522aee9e5c3000007fadf8633f45e3027826ea654965 40076 ocaml-curses_1.0.3-1.diff.gz
Files: 
 3c11b46b7c057f8fd110ace319589877 54053 ocaml-curses_1.0.3.orig.tar.gz
 e7a78b031601c959d9b9576592b0b33f 40076 ocaml-curses_1.0.3-1.diff.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)

iEYEARECAAYFAkspcxgACgkQir2bofsN/ptq8ACfW9SaNc51uJ3vS9Eb3lVjYS0k
NCIAnjqSaRMkawX5fQvCieyQoozVvvzP
=ShBo
-----END PGP SIGNATURE-----
