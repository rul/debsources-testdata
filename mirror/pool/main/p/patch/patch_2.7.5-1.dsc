-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: patch
Binary: patch
Architecture: any
Version: 2.7.5-1
Maintainer: Laszlo Boszormenyi (GCS) <gcs@debian.org>
Homepage: http://savannah.gnu.org/projects/patch/
Standards-Version: 3.9.6
Vcs-Browser: http://git.debian.org/?p=collab-maint/patch.git
Vcs-Git: git://git.debian.org/collab-maint/patch.git
Build-Depends: debhelper (>= 7), ed
Package-List:
 patch deb vcs standard arch=any
Checksums-Sha1:
 8fd8f8f8ba640d871bce1bd33c7fd5e2ebe03a1e 727704 patch_2.7.5.orig.tar.xz
 9208549344c913cf055622f7db9ae34f184a7d3b 8116 patch_2.7.5-1.debian.tar.xz
Checksums-Sha256:
 fd95153655d6b95567e623843a0e77b81612d502ecf78a489a4aed7867caa299 727704 patch_2.7.5.orig.tar.xz
 c5b9797658fdc1c150072fc9568279bd62c591b2fc43fa6d33750a9a4e8f0ddd 8116 patch_2.7.5-1.debian.tar.xz
Files:
 e3da7940431633fb65a01b91d3b7a27a 727704 patch_2.7.5.orig.tar.xz
 916187a32b65aafca7b41c49168e9351 8116 patch_2.7.5-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQIcBAEBCAAGBQJU+qUKAAoJENzjEOeGTMi/faUP/1LJbzWMeybAKEom1215Osg/
gbGKYlYPSHVuFad4pX4C6nTIFtHMSE27chf3geWy4LDvR81a025UV7sI4VU0ZBH/
/GU6LVEsWDZH/AgtoGVp5LVirJeMQNl4ulcyXAhXhT6jsrmKymck2q22nV1/FEIA
jkt8lzFtKX0OaSC9KuxKoGfhblzkBmKW7cDLWaj4PxsTJf1GBMrBSSu8bjLmiqOS
2Ua4Lo2Q9aw1O71Mddj25lJ2Cfovf+FY65CwtRhoFQt1ih1YMGAie75RKz6bZtbB
E6bU4VcXFFY/RAyP4p+dIVfF5Bg6ELzPQhkBGq45XtKcZsYjFkeiIdO5icm+0Vvp
geP+PMtGeFywNAE2Oulyh9jlVFAAzZHwGniw6gJi15mKYBuHfuobigV2e56EVYrW
y2wpYWO15YkAX58vSbqwYO7JgwHg8m5Y8WNaNVtyOx8ihWCN7HSvzlk61+Y1GQf9
j6d27ClHiZ64JW47B56fvRboCktpF+zRzTfeBv0Sd76KsPEBOTgW9yy0iB/1Nm/C
/wUibRAG4rN1JHdgu9NoQtSAqlwVTYLk+yvKhoXlKmKPTzkE+80aQiay2W2rHrif
jpHEtbz0jJ4+VX1dhuZ1RRf18W4mSM05zP5wD2NO7WneNEdkmT0BxpXLyZrVtVoz
yD0c8+RCvNCHPvp5VV8d
=9JeN
-----END PGP SIGNATURE-----
