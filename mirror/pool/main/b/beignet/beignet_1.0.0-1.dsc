-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: beignet
Binary: beignet, beignet-dev
Architecture: i386 amd64 kfreebsd-i386 kfreebsd-amd64
Version: 1.0.0-1
Maintainer: Debian OpenCL Maintainers <pkg-opencl-devel@lists.alioth.debian.org>
Uploaders:  Simon Richter <sjr@debian.org>, Rebecca N. Palmer <rebecca_palmer@zoho.com>, Andreas Beckmann <anbe@debian.org>,
Homepage: http://www.freedesktop.org/wiki/Software/Beignet/
Standards-Version: 3.9.6
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-opencl/beignet.git
Vcs-Git: git://anonscm.debian.org/pkg-opencl/beignet.git
Build-Depends: debhelper (>= 9), cmake, pkg-config, python-minimal, ocl-icd-dev, ocl-icd-opencl-dev, libdrm-dev, libxfixes-dev, libxext-dev, llvm-dev (>= 1:3.4), clang (>= 1:3.4), libclang-dev (>= 1:3.4), libgl1-mesa-dev (>= 9) [!kfreebsd-any], libegl1-mesa-dev (>= 9) [!kfreebsd-any], libgbm-dev (>= 9) [!kfreebsd-any], libtinfo-dev, libedit-dev, zlib1g-dev
Package-List:
 beignet deb libs extra arch=i386,amd64,kfreebsd-i386,kfreebsd-amd64
 beignet-dev deb libdevel extra arch=i386,amd64,kfreebsd-i386,kfreebsd-amd64
Checksums-Sha1:
 a2b0eb53e5f9a6055cd656531532a4c6ae03fbb0 827800 beignet_1.0.0.orig.tar.gz
 81734dbaaa365d78d5247f4a4bda0ad9e92bf763 8816 beignet_1.0.0-1.debian.tar.xz
Checksums-Sha256:
 e30c4d0f4c8917fa0df2467b2d70a4ee524f28d54c42c582262d5f08928ea543 827800 beignet_1.0.0.orig.tar.gz
 fa4603ca5772c838391997fd589ad86e3550ecd27740f7ad2487fe00b459a629 8816 beignet_1.0.0-1.debian.tar.xz
Files:
 bfd755904c332cdd285d6058f5f3de8c 827800 beignet_1.0.0.orig.tar.gz
 21678f71c716ef0a8033dabf3f290982 8816 beignet_1.0.0-1.debian.tar.xz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1

iQIcBAEBCAAGBQJUa1S4AAoJEF+zP5NZ6e0IGaEQAJgPoATGJWAPwa/su5y3uNtG
E0J63BUeJhyGP1IQHVSJDDAmTJzGINyUPI+wYvlj3fr5gZ5/JCyOObaWRP/VxNLX
oCB/nqntvllBukoGn5OkxK/R/KTt/I4VRJmkHpU549FN95du05IdciiuUj3LRibn
/PFUqE9qhzpzYG0My9oNZa5GUZAa2AyotHkx3zfYm16QFxETH5A8YgCfb7gfVN+M
sG70FrYLagVKJx9EMRCMaNKtZdrV8Kkg5RdL4ETcjERUPYBt6oHVfEph7IOAElwU
1yPAncF5j3mR8DGMTs9MlYsF3fYFYja0+85zTDXpOQUqO1aHzEhanFjo6rBrYCn1
2dN3/cjxX7I6qd72LS02BWq04BcGT+6gdTKjPtxS/xGo5JrkzGB38R0TF4EMEksY
af0PSxvk1dHKLF7Zv6xWQqfOY65jBS1gH6e/6f5tAhdfY83iDwzsv6rNAgYLwkp2
ykN5A2QOR+vLwJ8Nf14YyaJFxULYUjZ47fO4KZcSNq5Nj5dG4EYjK3ev02EVLxWK
jXY0N5MynqIP2j6ccNKJe0wC3iGaNlP2JKpfGC3mInosodDyCfzrST+AK98SJDxM
CEZibRMs46XCNW8WgYRsIUuGFdYedtKwDhh/LEIMB9AyjYP6M3kbaodl4DMl3V0f
sP3xnyZfCTxrMpkt8RVV
=GNx+
-----END PGP SIGNATURE-----
