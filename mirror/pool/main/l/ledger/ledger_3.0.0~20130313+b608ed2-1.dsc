-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: ledger
Binary: ledger
Architecture: any
Version: 3.0.0~20130313+b608ed2-1
Maintainer: Matt Palmer <mpalmer@debian.org>
Uploaders: David Bremner <bremner@debian.org>
Homepage: http://ledger-cli.org
Standards-Version: 3.9.3
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=collab-maint/ledger.git;a=summary
Vcs-Git: git://anonscm.debian.org/git/collab-maint/ledger.git
Build-Depends: debhelper (>= 8), cmake, libgmp3-dev, libpcre3-dev, libexpat1-dev, libofx-dev, libmpfr-dev, libboost-regex-dev, libboost-date-time-dev, libboost-filesystem-dev, libboost-iostreams-dev, libboost-serialization-dev, libboost-test-dev, man2html-base, python, texinfo, texlive
Package-List: 
 ledger deb utils optional
Checksums-Sha1: 
 ede3dd41e021462bf86cae710507cab03cef365e 860855 ledger_3.0.0~20130313+b608ed2.orig.tar.gz
 8c9f5245e3c7ecaacc66f57d7248f71c12f6dd6f 6180 ledger_3.0.0~20130313+b608ed2-1.debian.tar.gz
Checksums-Sha256: 
 5e14385476d89d49c2b18ebd1ee6a463022782c6cfa5ff84080ef588b5b89e62 860855 ledger_3.0.0~20130313+b608ed2.orig.tar.gz
 3b7b8a6d05682b94ec63294e6adaddb3c6550ec4defee9528c4e7a9f85ddaa36 6180 ledger_3.0.0~20130313+b608ed2-1.debian.tar.gz
Files: 
 cc7f05a35411a4a3181e2cbd02fe5b20 860855 ledger_3.0.0~20130313+b608ed2.orig.tar.gz
 d6f5c7f6d4b39be03a3c96ebd9e2c667 6180 ledger_3.0.0~20130313+b608ed2-1.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.12 (GNU/Linux)

iJwEAQECAAYFAlFEb9AACgkQTiiN/0Um85kPSgP+K5YFLBS4937EkOvrPC3q/XZM
TJegTxbKAYyiliTf/2JMP41vaIRYQdMNwYXjp/3c73XGkJN6op7poYjY5xPu7y8n
fehwh77o2DF8GS6X0OedP5UYHlxD0q3LzdkxoT/IjQQcNXuk353AUXPNPFnV/g6K
FG+zD76CyFZ4lAr55y4=
=coxV
-----END PGP SIGNATURE-----
