-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 1.0
Source: ledger
Binary: ledger
Architecture: any
Version: 2.6.2-3.1
Maintainer: Matt Palmer <mpalmer@debian.org>
Homepage: http://newartisans.com/software/ledger.html
Standards-Version: 3.9.1
Build-Depends: debhelper (>= 6), autotools-dev, libgmp-dev, libpcre3-dev, libexpat1-dev, libofx-dev, texinfo, texlive
Checksums-Sha1: 
 2959e2c589f8d3f73b0f764326e8c8be3bfe0726 655360 ledger_2.6.2.orig.tar.gz
 40bca027bafc1fe2a63619e1304e03a1700a88e1 54368 ledger_2.6.2-3.1.diff.gz
Checksums-Sha256: 
 52efebd43155603b2a33854264bb71e0f809f061729185b117d89ce72011ccd4 655360 ledger_2.6.2.orig.tar.gz
 afd33e2a6d57e314c9fd77d26a79db40b0f9f2bac3e8959fc8d4f82573391b19 54368 ledger_2.6.2-3.1.diff.gz
Files: 
 b2e6fa98e7339d1e130b1ea9af211c0f 655360 ledger_2.6.2.orig.tar.gz
 12be5c76e1616d881d5c479634c1309c 54368 ledger_2.6.2-3.1.diff.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.11 (GNU/Linux)

iD8DBQFNgux10i2bPSHbMcURAlilAJ909+GADn1G2tYyaDkiwOCeChxKDQCglUrN
vz6yAlRzUGAVTrd3U5Hni18=
=B7ww
-----END PGP SIGNATURE-----
