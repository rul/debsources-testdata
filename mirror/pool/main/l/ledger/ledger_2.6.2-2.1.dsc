-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 1.0
Source: ledger
Binary: ledger
Architecture: any
Version: 2.6.2-2.1
Maintainer: Matt Palmer <mpalmer@debian.org>
Homepage: http://newartisans.com/software/ledger.html
Standards-Version: 3.8.3
Build-Depends: debhelper (>= 6), autotools-dev, libgmp3-dev, libpcre3-dev, libexpat1-dev, libofx-dev, texinfo, texlive
Checksums-Sha1: 
 2959e2c589f8d3f73b0f764326e8c8be3bfe0726 655360 ledger_2.6.2.orig.tar.gz
 a480081d2c528117dd11a4de59f632fb73598247 52089 ledger_2.6.2-2.1.diff.gz
Checksums-Sha256: 
 52efebd43155603b2a33854264bb71e0f809f061729185b117d89ce72011ccd4 655360 ledger_2.6.2.orig.tar.gz
 265f3bec2b2607d4634518b918a789371ee9b6b47b53e80f4331d4b33ec16406 52089 ledger_2.6.2-2.1.diff.gz
Files: 
 b2e6fa98e7339d1e130b1ea9af211c0f 655360 ledger_2.6.2.orig.tar.gz
 d036530bbe99fd257e902c13b0d35d62 52089 ledger_2.6.2-2.1.diff.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.9 (GNU/Linux)

iEYEARECAAYFAkvTSnYACgkQlAuUx1tI/6543wCeN8v920cSQNVByDE5Qz/ux8V9
U7oAoKeZ6v2AQRzq9NbWoMfeYtgQ7YN4
=tIyY
-----END PGP SIGNATURE-----
