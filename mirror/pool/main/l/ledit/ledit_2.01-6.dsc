-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 1.0
Source: ledit
Binary: ledit, libledit-ocaml-dev
Architecture: any
Version: 2.01-6
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders:  Samuel Mimram <smimram@debian.org>, Stefano Zacchiroli <zack@debian.org>, Stéphane Glondu <glondu@debian.org>, Mehdi Dogguy <mehdi@debian.org>, Sylvain Le Gall <gildor@debian.org>
Homepage: http://cristal.inria.fr/~ddr/ledit/
Standards-Version: 3.8.4
Vcs-Browser: http://git.debian.org/?p=pkg-ocaml-maint/packages/ledit.git
Vcs-Git: git://git.debian.org/git/pkg-ocaml-maint/packages/ledit.git
Build-Depends: debhelper (>> 7.0.0), dh-ocaml (>= 0.9~), dpatch, ocaml-nox, camlp5 (>= 5.12)
Checksums-Sha1: 
 97ee7ee60fb8e66cdcc8fc61339e8ab616bcd2df 22633 ledit_2.01.orig.tar.gz
 acbe0c5cf170c28362cd4b11068e0d1cee52f7b4 5664 ledit_2.01-6.diff.gz
Checksums-Sha256: 
 6e754afe923c4682328f3d66e065f04d346ebbffd426bd1a8219f5dbe3517344 22633 ledit_2.01.orig.tar.gz
 fe50557b39dd906c061cba429e41535a941ec95b40549e9024b637bfbda8eeab 5664 ledit_2.01-6.diff.gz
Files: 
 24faa563dff1091aea2e744b1ec15fbb 22633 ledit_2.01.orig.tar.gz
 33d10f2eef07d46be5b75a5c75fe593b 5664 ledit_2.01-6.diff.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)

iQIcBAEBCgAGBQJLcxjXAAoJEHhT2k1JiBrT2T4P/RRe/0WLP2k+oSIPrfnMRmE+
DI+b1DfJGfSJMw2nVA8O/HFk9bv7FbDzEpN6QfV5+D4g2rwVt1VfV1+H7NUK6L2z
bEW5DnSI5g0Ze5+iBh9VDMLffPZCukXyjoy820LWeVekjXvcUbNaPNuLDsWkOwRd
9xUF+235GVpwT62TSMaaSjXSUVPP5DNkL9KfcJZ/BEbCY1kpsKyV3Rxc5HisbVHP
5TUHwukAB8lXUPxlLa08PadDOMlrwaeOgWDOexHnpehdb+c57J+OrafjAdyvge5m
+289dMmbSoP7bqUNeAvGfFdFDcwD1WnMT5jXuZNnuEIskj1DO+YsgqFuSMCQXhB/
tL7gwg6Lf7X5d9OuZ/kyM6crgwJAF2fyGotdcnyxaF5bReKyB3Yf2pT/OC3U1JP9
px4655Wbc9TPoMbCfa2jq0CHKogKTDoC45wELR034gHxoC+B8IwO7ldFm5qgKcgj
/vVN1pZiUeYSsmwwaK9VK7OU7frjIXF6563YUOm6RYQWTTimcAZdYsAjVer+TB1v
ZNqZsHSAyVeZDqiCDutUC99zIbYCPuacvEvlNmzUXsf7Vcjk311Y48ZI2KR65N3L
qptqWzuvC0t5yUHJtgIkFg/avYfLM+E/M6LKyU1Pgf82DvVVTncbhLyWD1CoX7pa
puryVhVMMSCtXOaqnQT3
=3wx6
-----END PGP SIGNATURE-----
