-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: ledit
Binary: ledit, libledit-ocaml-dev
Architecture: any all
Version: 2.03-2
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders:  Samuel Mimram <smimram@debian.org>, Stéphane Glondu <glondu@debian.org>, Mehdi Dogguy <mehdi@debian.org>, Ralf Treinen <treinen@debian.org>
Homepage: http://cristal.inria.fr/~ddr/ledit/
Standards-Version: 3.9.1
Vcs-Browser: http://git.debian.org/?p=pkg-ocaml-maint/packages/ledit.git
Vcs-Git: git://git.debian.org/git/pkg-ocaml-maint/packages/ledit.git
Build-Depends: debhelper (>> 7.0.0), dh-ocaml (>= 0.9~), ocaml-nox (>= 3.11.1-3~), camlp5 (>= 6)
Package-List: 
 ledit deb editors optional
 libledit-ocaml-dev deb ocaml optional
Checksums-Sha1: 
 8fef728f38e8d6fc30dd5f71dd5b6b647212a43a 24319 ledit_2.03.orig.tar.gz
 ab7a2b0f83817d140a95713a17516a7bfc857476 6576 ledit_2.03-2.debian.tar.gz
Checksums-Sha256: 
 ce08a8568c964009ccb0cbba45ae78b9a96c823f42a4fd61431a5b0c2c7a19ce 24319 ledit_2.03.orig.tar.gz
 bb8cd6f4726f3b70e426748e1ab1f8b54fc069d099d894c5a26c266ff31f3e1f 6576 ledit_2.03-2.debian.tar.gz
Files: 
 3a70ee7d5d5e2dfb905a1ac2e1e60276 24319 ledit_2.03.orig.tar.gz
 bac801cb0a8a6beeaf9a8cd091519af3 6576 ledit_2.03-2.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.15 (GNU/Linux)

iQIcBAEBCgAGBQJSoXZ7AAoJEHhT2k1JiBrTitEP/j3bSE5vxPnXDE3zDs48hMsu
jQx1YKOfy99zSv/sUxAdbec/xuEsZkJKBCdlwLr4T05OEOw1ljGY60GfT46/nQ9i
6vtPjo50WaXEHRXZpoLU553lTDJam3UQtXjuCMbTkq1AhHtOc0s15inXSkXdKoS5
fsiK86jymkQeBuEpckSAB0Y2AW4E+Db6gC6UAF00UXu8Qbhuotm7ZSqNOeYq08eM
XEIG8MJOz1LDgp+bYpMRFZZ3LbfA3iM/+r/QpS0mwU3lGsW9U1JXfxJX2fmSugcM
PR+7GZg83HV95C253ak+sbXEjcrk2nPu2v0uE3N/FWwk4i+MXhoY2c837sOxcrJq
KvH9h3zPtxmkXgbXFQ6oWyCBUQPqcz6nK3XtjKBEsJ1kkhRD4iN842Sb9GK/hfHP
eNxVQ5YZF5VJM3J+PkxzTYA7F6gC2yRJblcD4xdCpYKhslnRdOuCBnceJlmDKYMj
wnvR9RjW6RvrR19LbLdVKB7Ca2vfxKpBfQWQbMRZ+/3nsln1FtUqs17aIqNIvS9Q
0QxM+KxjppoIMHPys+KACZRo77teKRRE0u/OVFtvPF2s7hk48tGYtoJctDkb3XkE
bmxl47tThLGsAOClBH+IK4Q5ECiTbVPGBwbLMu0wY6lvcR+OIiO73ZL8lCOFDsJ6
SqNpmraOPTiOAIOtFgst
=QQNi
-----END PGP SIGNATURE-----
