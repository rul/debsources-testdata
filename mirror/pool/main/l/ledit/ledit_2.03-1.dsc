-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

Format: 3.0 (quilt)
Source: ledit
Binary: ledit, libledit-ocaml-dev
Architecture: any all
Version: 2.03-1
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders:  Samuel Mimram <smimram@debian.org>, Stéphane Glondu <glondu@debian.org>, Mehdi Dogguy <mehdi@debian.org>, Sylvain Le Gall <gildor@debian.org>, Ralf Treinen <treinen@debian.org>
Homepage: http://cristal.inria.fr/~ddr/ledit/
Standards-Version: 3.9.1
Vcs-Browser: http://git.debian.org/?p=pkg-ocaml-maint/packages/ledit.git
Vcs-Git: git://git.debian.org/git/pkg-ocaml-maint/packages/ledit.git
Build-Depends: debhelper (>> 7.0.0), dh-ocaml (>= 0.9~), ocaml-nox (>= 3.11.1-3~), camlp5 (>= 6)
Package-List: 
 ledit deb editors optional
 libledit-ocaml-dev deb ocaml optional
Checksums-Sha1: 
 8fef728f38e8d6fc30dd5f71dd5b6b647212a43a 24319 ledit_2.03.orig.tar.gz
 8d3bc0c6c3c6b02ded9a3f6cde437071de874056 5866 ledit_2.03-1.debian.tar.gz
Checksums-Sha256: 
 ce08a8568c964009ccb0cbba45ae78b9a96c823f42a4fd61431a5b0c2c7a19ce 24319 ledit_2.03.orig.tar.gz
 a6899d3e8caeab203510146590c0668644c65672cf51aaac64e71c74997a27f3 5866 ledit_2.03-1.debian.tar.gz
Files: 
 3a70ee7d5d5e2dfb905a1ac2e1e60276 24319 ledit_2.03.orig.tar.gz
 f411cf4b514c9e4e8474aae8455225ba 5866 ledit_2.03-1.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.11 (GNU/Linux)

iQIcBAEBCgAGBQJPU6tSAAoJEHhT2k1JiBrTY7YP/2q2J7CDtso/OHwTRriyllZj
eX52ll0Zd3uHiM4oacliWTHy9+/AqvVJdkXtHy4StOjpuWkH3K54aKyfe1S/pA5S
Bzu6YTja9e73S2zsw/OrjIy9EbneunaZqgyWygD88lxPw98vsWyT+TX7KbmJmDWh
mFetD9FeNEFUYYbG0JS3aQ9WtDIXyvFOtbiq8/S7/k7UHxmtOLhZj5/ftcmYWqgW
acbLmk5g5A3aFBdaNC1kMBKFYXsTjPBU56LaVbT0pxdlsIX8hugYsbq0YBcnIiLP
rk/b+/iS5SOlItugbxO0V3y6CEVVXmeg2aUIhfg02iZ6jvHWfMeAIcUQkor18l9O
S4Sq738znb9AD8nOxtMkWgGf1AJXxQIK6tAZLOhiCTusKIbSqWsOQ7TcCUQbnMvv
7qCYRGwQhMr9mrM4HWT7bFac/XNxE2U7ULMwi2gXCcyF32nylu2pcxm9l4CmM/bt
apRpFIUg1mBK67x/7ZmsPnXZIHl7S5IKGq3ZIZs+/j7Uon4ga/N6ZcP59oABCSUZ
ps3u5C1NH1SFWQexCwCSQyVHzcD1+nKP2sUymGqQKt0lpxWSsWrbwm8M0sGEPsv3
HlH98S4/8WzoHCo5XQg3cYmMRnvXQRXRfNi4Tn/4mamNaMyrcFvgpJdBu2lnUQdD
rucqln0JgqSNNX/YhPA6
=nQt8
-----END PGP SIGNATURE-----
