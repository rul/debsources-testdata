-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 1.0
Source: gnubg
Binary: gnubg, gnubg-data
Architecture: any
Version: 0.90+20091206-4
Maintainer: Russ Allbery <rra@debian.org>
Homepage: http://www.gnubg.org/
Standards-Version: 3.8.3
Vcs-Browser: http://git.eyrie.org/?p=debian/gnubg.git
Vcs-Git: git://git.eyrie.org/debian/gnubg.git
Build-Depends: debhelper (>= 7.0.50~), autoconf, automake, autotools-dev, bison, dblatex, docbook2x, docbook-xsl, flex, libcairo2-dev, libcanberra-gtk-dev, libfreetype6-dev, libglib2.0-dev, libgmp3-dev, libgtk2.0-dev, libgtkglext1-dev, libncurses-dev, libpng12-dev, libreadline-dev, libtool, libsqlite3-dev, python-dev, texinfo, xsltproc
Build-Conflicts: autoconf2.13, automake1.4
Checksums-Sha1: 
 c55365122f007b142eb9bbe60ece0a7ccc3ef748 14117278 gnubg_0.90+20091206.orig.tar.gz
 2cfa61fc182c3b9df286889bfef967e299978d0b 23259 gnubg_0.90+20091206-4.diff.gz
Checksums-Sha256: 
 c854690909491653a0219237a30bb2c1ff49870f6360c458ac64a7075a7ad2e0 14117278 gnubg_0.90+20091206.orig.tar.gz
 5578e136fe55c86940082dfcc478330946447661fd313547ffa8264e545190dd 23259 gnubg_0.90+20091206-4.diff.gz
Files: 
 dd5efa3b211ee97604a59a229ab7fb4b 14117278 gnubg_0.90+20091206.orig.tar.gz
 6a420c36e5687dfbcaa14d333fb59ec4 23259 gnubg_0.90+20091206-4.diff.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)

iEYEARECAAYFAktY4eoACgkQ+YXjQAr8dHY0HQCfS2echvVcVmBfGZMyxQUdGLSu
/hEAoJXWZkdpq3IzHldLihjfF2KnQ3tq
=o+aC
-----END PGP SIGNATURE-----
