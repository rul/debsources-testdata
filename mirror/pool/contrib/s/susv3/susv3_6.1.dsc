-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 1.0
Source: susv3
Version: 6.1
Binary: susv3
Maintainer: Jeff Bailey <jbailey@raspberryginger.com>
Architecture: all
Standards-Version: 3.6.1
Build-Depends-Indep: debhelper (>> 4.0.0), cdbs
Files: 
 c52befcab03725af6d2d25d8a5f5f619 1562 susv3_6.1.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.0 (GNU/Linux)

iQCVAwUBQi1nFftEPvakNq0lAQKvegP/d5KYE8xmpI5vMCM3yDDrWy2v+sM/IpoZ
0zMVrubqcgzxKtoD093ivLPI5mcmOACAXun574XHS71sgEkX8p6S4DaDZIPi0u/q
xBb9ka6D/PYtCmsKC7hr5eJGNnkgAYc8iey19T++LgZYn/kNK9uUzE5QO8+1hmst
S2QDM46Q/4s=
=uQbr
-----END PGP SIGNATURE-----
