-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: nvidia-xconfig
Binary: nvidia-xconfig
Architecture: i386 amd64
Version: 304.48-1
Maintainer: Debian NVIDIA Maintainers <pkg-nvidia-devel@lists.alioth.debian.org>
Uploaders: Russ Allbery <rra@debian.org>, Andreas Beckmann <debian@abeckmann.de>,
Dm-Upload-Allowed: yes
Homepage: ftp://download.nvidia.com/XFree86/nvidia-xconfig/
Standards-Version: 3.9.3
Vcs-Browser: http://svn.debian.org/wsvn/pkg-nvidia/packages/nvidia-xconfig/
Vcs-Svn: svn://svn.debian.org/pkg-nvidia/packages/nvidia-xconfig/trunk
Build-Depends: debhelper (>= 9), m4
Package-List: 
 nvidia-xconfig deb contrib/x11 extra
Checksums-Sha1: 
 49b55a0419913bb0cce3f531b8b6627c05df4f01 101479 nvidia-xconfig_304.48.orig.tar.bz2
 80e0f163e0ae57c63380cb8ec1b379c80098480f 5730 nvidia-xconfig_304.48-1.debian.tar.gz
Checksums-Sha256: 
 9cab6c0f76f95e746e22dcb93f7c08a9f7cfb991399f8786a1b938948585ef06 101479 nvidia-xconfig_304.48.orig.tar.bz2
 0bc48b10f7987fc125d4efd1b9fdf2807847b65eaa3efdf5ac7caab8a5f688f5 5730 nvidia-xconfig_304.48-1.debian.tar.gz
Files: 
 b634afe72c708eba359fc7a42b2e5b4e 101479 nvidia-xconfig_304.48.orig.tar.bz2
 47e33bbf38e587ce931b60e46574723f 5730 nvidia-xconfig_304.48-1.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.12 (GNU/Linux)

iQIcBAEBCAAGBQJQUGocAAoJEF+zP5NZ6e0ID1EQALNvZbVtGxsQ3YHANd7478DW
v/CofPJMKYkFTRwnTFvH5wKZgT29Ofw+eWM5XlEpQLHVIl/7stDs52FzHecX6SIs
3DOmGAKd2SEnDhoSv1oqNYzogQ7mkQabU1AVrPcjpvEfibUOIdFj2OwVMhEhMsfb
+H8eW2PqvcR0VP7+E/XsjAbB0UBWrldXjkm08OnoK6g6qv+KoHIgp/+sMgt5xN0I
gIKhZP5kCN9BUS5q4jUii9yY1pd3olI9BqPko7j0SfSLsErjjszd9yvHJKkfHZkh
4CDHrQvx5oPji2rjhshjaifIoVEK8XEYLxtAlG5u0EN6vTXiqAlyO2PooHrztARK
Tk53Y7H/kb9H0Q3MUI0mqYAqZ7N0fY3RQCqrN1iHhB1WA7vj/25aGkiarTWA/EeG
+GKhdGHp6lFdrlKJ11eaHJh/1l3dMB+NIwNgORKki5WhUHj09q+KNhOp9YjbWf2L
Yr4DeixQV14AXs53w6BQvLsEnPVgi8wTxuUxfkb1Ad2SpjRIUyPLOlKkarqfKNQK
8Y9e0y+FQ4NKrub5HEhelxxoFZtfiJANt2gfYYFoM85t9Pa3g9fgLs/bAl0dL9e0
dfGo9nhObS0KA2DpGebJaGd/o3dSWf1e2aQ3iQlk0hA9IYfs72WgjLkWCPLL6qZW
/G9ei3k/o2rOj9VfevZi
=fmZ2
-----END PGP SIGNATURE-----
