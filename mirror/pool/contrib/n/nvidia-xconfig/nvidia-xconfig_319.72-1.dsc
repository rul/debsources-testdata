-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: nvidia-xconfig
Binary: nvidia-xconfig
Architecture: i386 amd64 armhf
Version: 319.72-1
Maintainer: Debian NVIDIA Maintainers <pkg-nvidia-devel@lists.alioth.debian.org>
Uploaders: Russ Allbery <rra@debian.org>, Andreas Beckmann <anbe@debian.org>,
Homepage: ftp://download.nvidia.com/XFree86/nvidia-xconfig/
Standards-Version: 3.9.5
Vcs-Browser: http://anonscm.debian.org/viewvc/pkg-nvidia/packages/nvidia-xconfig/
Vcs-Svn: svn://anonscm.debian.org/pkg-nvidia/packages/nvidia-xconfig/trunk
Build-Depends: debhelper (>= 9), m4
Package-List: 
 nvidia-xconfig deb contrib/x11 extra
Checksums-Sha1: 
 00894ccb00de76900cc1ab3cdb19a1a4207ca0ff 101999 nvidia-xconfig_319.72.orig.tar.bz2
 533e9601a7de2cbcf5def116834899e681fc3783 6355 nvidia-xconfig_319.72-1.debian.tar.gz
Checksums-Sha256: 
 55c8d63e1c5367c806e76e0a1e0275cb2ede3e45adc756d41b63e8c3ae442300 101999 nvidia-xconfig_319.72.orig.tar.bz2
 21f683a86e16fa3ea37f650b20ae805f335a637a4f42f797c3738dbbeb1229c2 6355 nvidia-xconfig_319.72-1.debian.tar.gz
Files: 
 8317cb694f1659f45949dc25d8af3fef 101999 nvidia-xconfig_319.72.orig.tar.bz2
 a30ee3abd648d5a43b80dcdcd05877ff 6355 nvidia-xconfig_319.72-1.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.12 (GNU/Linux)

iQIcBAEBCAAGBQJSfkO8AAoJEF+zP5NZ6e0IEdcQAJO+i3E9pkileisdU/pskqOh
Rmnlxs2Fz5CyE9XTpRae/T59MiOKgvCO1nWkk2oJ1owJjtuidUMVeQ7mvCb3TzVy
J9kWWeFZxHXEZQj7X+pO+nDMUCkTP+VgjeKuN2FH3U8pJnmw9K98R0H80YxeiSpk
yX03EBzPZhnDedEX69EQe8Incwpn+45pedxC+0qm2iC9lEEPcwF52rFSJtkmc1Ui
uDs0t65MCALJ4kjwtdWd+zbRw1uKiI6jYOSrt1PQfSQyryxYIvXlS7I5itLK++i1
rVQfbau6yiPVy8hcI0MnJlzfZU4hyT5uXHF1OzmtRzqh/aJK4CPf/4qG8m59hsZY
aLTCQQCDSMgP8FWI1N6EasAsBQm4rF1nxF3ijU6xTWg67Kkc9JgU5nq/XLK7SHtp
YW0qKFAp9d3TJxrkr70PZxg5Th37FRykilMsab5GB1RsownHKfRp9UbPvMSr23Ui
XbnEmhrKJq0eMvcGhMO2BTbKDuYmN2IK88b4Sq3w013h7aBnHJD6bc6y9R00yZCx
YrCw9MtfXfKyG9TqSdQj/q3O/uQv5LSsR7Vdk1RJV/VCav2ywkZD3xXCHNUM5vEX
YZiZlKMWlP2e72Jc8uFLYYRgWRucpTJfJi2bDX1h6cpCDh6VQrhheHriyULOXpNB
10brJDBCtO2Xd0bGV0hP
=iLlB
-----END PGP SIGNATURE-----
