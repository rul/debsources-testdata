-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (quilt)
Source: nvidia-xconfig
Binary: nvidia-xconfig
Architecture: i386 amd64 armhf
Version: 319.72-1~bpo70+1
Maintainer: Debian NVIDIA Maintainers <pkg-nvidia-devel@lists.alioth.debian.org>
Uploaders: Russ Allbery <rra@debian.org>, Andreas Beckmann <anbe@debian.org>,
Homepage: ftp://download.nvidia.com/XFree86/nvidia-xconfig/
Standards-Version: 3.9.5
Vcs-Browser: http://anonscm.debian.org/viewvc/pkg-nvidia/packages/nvidia-xconfig/
Vcs-Svn: svn://anonscm.debian.org/pkg-nvidia/packages/nvidia-xconfig/trunk
Build-Depends: debhelper (>= 9), m4
Package-List: 
 nvidia-xconfig deb contrib/x11 extra
Checksums-Sha1: 
 00894ccb00de76900cc1ab3cdb19a1a4207ca0ff 101999 nvidia-xconfig_319.72.orig.tar.bz2
 abae42490371357965e84555a8b68af8ad30e60d 6328 nvidia-xconfig_319.72-1~bpo70+1.debian.tar.gz
Checksums-Sha256: 
 55c8d63e1c5367c806e76e0a1e0275cb2ede3e45adc756d41b63e8c3ae442300 101999 nvidia-xconfig_319.72.orig.tar.bz2
 c8961a6d62a4e31af6764812eca8738e40a9e5d50db43999c241a47a9da5f01a 6328 nvidia-xconfig_319.72-1~bpo70+1.debian.tar.gz
Files: 
 8317cb694f1659f45949dc25d8af3fef 101999 nvidia-xconfig_319.72.orig.tar.bz2
 d67f5e1571a7781d5e1f210dd2bfb005 6328 nvidia-xconfig_319.72-1~bpo70+1.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.12 (GNU/Linux)

iQIcBAEBCAAGBQJSkN09AAoJEF+zP5NZ6e0IAEYP/iAvrrz1+yWeo1+XJTo5Wa/B
rdrawGwH3TomaDygX9qSbprS+2BdwNXDw+Y0m5kIJUONxuBSfTIIqtqqBJUYiFDa
tFVO0K7USPvTW7W0xehkrcL6jw90fp5O6Nok9QU3fdteQl+2U130ZAf0K5ptbrtp
H/j7jG6n+1Cf4WBN8BFjOt6CSQGleQLVqibwlb7vV2pIerM2iCr/Kspw3/dCuNdl
Nn08vFBoLMBi52BBU0ixv9K4MXm3y9mq/YSTu10CI5hSFefOk05/R4E43A2WCLY7
YxVeocdY4XtrI3kT/Z7DUpFzys8HV4yDPJlqnmbXLYFYkEQpWSfhPj1QVsVjnyEq
FMvHibVY+ag3gPvzHb1r0cVh10M+Ggopu1DWM8GqIhBrHFL26cbnBIRTnq33shpm
F3FIYr8/BJPyAO5r+FHKaGr++3cA+rmA4lDuDSNNblHW8PGOyH2GMelf+Kyhyasf
W8pOG157Nc2+cxVBC4C/z3GQt7FkdPate5zoT+UVhZq+fxNdkB4dMchJ0cWWdeAa
RZ/R/RBX/ZGfXFaE1nGrj6tP8bGlKGxSCj8WwtsBtrfL7/byoj4MbEGrEi6VNutC
RjYoHGYdAsD0dBrimEz+xKRRssw6sVQrO38Gll36MaKAvXqn/euvn7aZhwoiXn4a
icPMBj+/DLl1zj7SutAX
=CW2A
-----END PGP SIGNATURE-----
