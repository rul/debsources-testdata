-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA1

Format: 3.0 (quilt)
Source: nvidia-xconfig
Binary: nvidia-xconfig
Architecture: i386 amd64
Version: 195.36.31-1
Maintainer: Debian NVIDIA Maintainers <pkg-nvidia-devel@lists.alioth.debian.org>
Uploaders: Russ Allbery <rra@debian.org>
Homepage: ftp://download.nvidia.com/XFree86/nvidia-settings/
Standards-Version: 3.8.4
Vcs-Browser: http://svn.debian.org/wsvn/pkg-nvidia/packages/nvidia-xconfig
Vcs-Svn: svn://svn.debian.org/pkg-nvidia/packages/nvidia-xconfig/trunk
Build-Depends: debhelper (>= 7), m4
Checksums-Sha1: 
 6c0600237dd10b84b6e4066d03194d40311980f5 121781 nvidia-xconfig_195.36.31.orig.tar.gz
 1df3d3f236b43cecb6238dbcf91cc1f8fb6280bf 4911 nvidia-xconfig_195.36.31-1.debian.tar.gz
Checksums-Sha256: 
 b83a9a91d27d36a2b8053f9a88bf46748e8ab01387362b54467c5e0e0bca1d52 121781 nvidia-xconfig_195.36.31.orig.tar.gz
 8d1aa2380e3f2c49db2a3113c3443f9050c43f4a08306baa33fc0695f25b1b84 4911 nvidia-xconfig_195.36.31-1.debian.tar.gz
Files: 
 e9925ef1df412c645437f564c4b2a635 121781 nvidia-xconfig_195.36.31.orig.tar.gz
 a637663bc8e7a08a239c523b0b19d6fb 4911 nvidia-xconfig_195.36.31-1.debian.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)

iEYEARECAAYFAkwUdQsACgkQ+YXjQAr8dHbXygCgkFK+tJteTCQLEFDsErVXluOL
M/QAoJORlGUOGohY3CtmwIlp25zieJ1/
=YiUH
-----END PGP SIGNATURE-----
