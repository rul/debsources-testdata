-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (native)
Source: nvidia-support
Binary: nvidia-support, nvidia-installer-cleanup, nvidia-kernel-common
Architecture: amd64 i386 armhf
Version: 20131102+1~bpo70+1
Maintainer: Debian NVIDIA Maintainers <pkg-nvidia-devel@lists.alioth.debian.org>
Uploaders:  Russ Allbery <rra@debian.org>, Andreas Beckmann <anbe@debian.org>,
Standards-Version: 3.9.5
Vcs-Browser: http://anonscm.debian.org/viewvc/pkg-nvidia/packages/nvidia-support/
Vcs-Svn: svn://anonscm.debian.org/pkg-nvidia/packages/nvidia-support/trunk
Build-Depends: debhelper (>= 9), po-debconf
Package-List: 
 nvidia-installer-cleanup deb contrib/misc optional
 nvidia-kernel-common deb contrib/kernel optional
 nvidia-support deb contrib/misc optional
Checksums-Sha1: 
 76813a2858226c818761e29648b89ba643b807c9 38079 nvidia-support_20131102+1~bpo70+1.tar.gz
Checksums-Sha256: 
 ec71f52f29b99201c9155b56cc19350e47453fc64d05678244f2d314f53f55d5 38079 nvidia-support_20131102+1~bpo70+1.tar.gz
Files: 
 9d433d828125a2190e844d58ebfb4d8b 38079 nvidia-support_20131102+1~bpo70+1.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.12 (GNU/Linux)

iQIcBAEBCAAGBQJSkPyyAAoJEF+zP5NZ6e0IUBMP/AuME/DkZQMVxrmHIEwVhfFg
4Y0TLZQ3poBjlB5mm5O05ciTz0sL7tR8d9MkUG7ftupniuCAsiLINkkBzyi11/et
/LeNYO2F77zcwrYFtZq8/m1tq1302hRCJB6po8ruJRq0wEqesRjbmKZgmoOrmc7p
TY1pqeBW08LsSvQkP5Zu3yPPqUGPFgmIBL4C5+lt/mHFBvxGNSiA7aViZd0hES9S
tbOiPNq9BLHjFqz6f2h6G9rAi9YXagOsOzEJn967kRUAiwReY+oaU1zDuVJdt3aB
tAds9rqMVIfKg/GQsHpvk51ytn4LPKw0xzvlbT0cPWYctd/0/jFzwvQao5AlE1tG
K6GuCOgt+61cPqh3OvIVwJUdgLIPqdw4Br43mYROz9gKR4LPEWP4bO6z5ZJszFiL
hleLn4N3ismCc7JiJVMezVI0MyNv2pusuc/Mi5N6aYsiP3D7v69qC0PEC/CKkm+o
dnl1Hzt6yJAYT8VreiuOXt4plYo+Nnba/14yN7dnVXJkPYRPi4tbsAtrAHBioHlr
I8WnddR8a/Ah1DGwawfFFiGvH7wKsYPsy0W8BZxEIvFT8zdsWgq/HOuvTbzAJIzJ
BIsE2D4ce55GYj1B7ziRgmsWfezeDC4ZBry7K81qxlHwf5Nr4rrp6b8L5jSW3Skx
XMnfgh1BnWmv+beHG9XR
=T1t1
-----END PGP SIGNATURE-----
