-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (native)
Source: nvidia-support
Binary: nvidia-support, nvidia-installer-cleanup, nvidia-kernel-common
Architecture: amd64 i386 armhf
Version: 20131102+1
Maintainer: Debian NVIDIA Maintainers <pkg-nvidia-devel@lists.alioth.debian.org>
Uploaders:  Russ Allbery <rra@debian.org>, Andreas Beckmann <anbe@debian.org>,
Standards-Version: 3.9.5
Vcs-Browser: http://anonscm.debian.org/viewvc/pkg-nvidia/packages/nvidia-support/
Vcs-Svn: svn://anonscm.debian.org/pkg-nvidia/packages/nvidia-support/trunk
Build-Depends: debhelper (>= 9), po-debconf
Package-List: 
 nvidia-installer-cleanup deb contrib/misc optional
 nvidia-kernel-common deb contrib/kernel optional
 nvidia-support deb contrib/misc optional
Checksums-Sha1: 
 f77d59aad072c62872ac8eca0845245754acaee0 37898 nvidia-support_20131102+1.tar.gz
Checksums-Sha256: 
 d4ad85fe552eac9128c1a2749c9d8f1cca4efc19de77bc05741e272f3f6ebe81 37898 nvidia-support_20131102+1.tar.gz
Files: 
 de0f305b7de4e991b1f7955176f039f7 37898 nvidia-support_20131102+1.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.12 (GNU/Linux)

iQIcBAEBCAAGBQJSdWbBAAoJEF+zP5NZ6e0IMroP/03augV/kMfWN244RbJCz7cP
CaZNCZdkY9a1S00ULp2kVSLn+FTeH2Iwxk4oc+TsISZ8/MjDplZiIlB7wyvWdTRk
DSoY4Xi+KO5pWZsHf4xw+maqqP8MxuR3jfzwj4wULpl1lroXPI4dwOC/TrLr/7UT
0VOT43noyt1arxaZoDFM0M85kG+SaaDGXChrGJo+ZCE/ypOcQ/r/Sfiq1NTyzxp2
kgeelYktZM26auEWxhQNMANYwi55CFaOQMAwcVSVSygGLLppoitRZB7ZBWs9sKqR
G4JPkMvdvmWjasXrPUJcA/yH1e9ZxsRyvoU9OZhsPz8LoU93pfYQb7FyBEZtWZ4R
qeQ5wI2diXb057hO50icrviUTo6WkHbKHjyNf+HgUX8VG7anmyT3W9FY+FTfppNW
UbMDP6V1aPCMpdF8khPwP3XG3KttJBppuToD/JouTbjBCgp53WZufoIKTDeISWOg
eEUaFZeG0dkKFq8jp5oSsN9gmlGv6EWuTQ9lbFqaN+QP7fieBOFvFx+FqQDUqNKa
BQ5zxzLhv0vtCYqAIV2773d1v4y5UcmqLupfqOQZT4k0kK7SQvxDN+KIAQMbPUmR
lT+8OFLBKj1q+3V6bwFAO3hdOaC6wudP9jtKSIEzkX+baKGxU6MjT0fh0O03OtzF
hqiow0MRCSZL9Ux6FY4f
=HQNY
-----END PGP SIGNATURE-----
