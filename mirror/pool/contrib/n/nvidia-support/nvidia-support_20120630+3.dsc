-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (native)
Source: nvidia-support
Binary: nvidia-support, nvidia-installer-cleanup, nvidia-kernel-common
Architecture: amd64 i386
Version: 20120630+3
Maintainer: Debian NVIDIA Maintainers <pkg-nvidia-devel@lists.alioth.debian.org>
Uploaders: Russ Allbery <rra@debian.org>, Andreas Beckmann <debian@abeckmann.de>
Dm-Upload-Allowed: yes
Standards-Version: 3.9.3
Vcs-Browser: http://svn.debian.org/wsvn/pkg-nvidia/packages/nvidia-support/
Vcs-Svn: svn://svn.debian.org/pkg-nvidia/packages/nvidia-support/trunk
Build-Depends: debhelper (>= 8.1.0~), po-debconf
Package-List: 
 nvidia-installer-cleanup deb contrib/misc optional
 nvidia-kernel-common deb contrib/kernel optional
 nvidia-support deb contrib/misc optional
Checksums-Sha1: 
 7085858fd3171296f6010ad8b76177be34678ae5 35212 nvidia-support_20120630+3.tar.gz
Checksums-Sha256: 
 4685ee971c424475e6dc95399d3d8bf2b2376c1ecdd05b522510e31f82b503aa 35212 nvidia-support_20120630+3.tar.gz
Files: 
 05a21bddaa3a6dca2ae1fde2764cd210 35212 nvidia-support_20120630+3.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.12 (GNU/Linux)

iQIcBAEBCAAGBQJQG8PTAAoJEF+zP5NZ6e0IAk0P/RNdXQOxmJRdfy6LZlKvUmdP
tIBKagDQ7Qliyc23cYM5b6Nx+gy7oavo6cO77+Wjt7BIs8RxoqEedG2BPcLMX4fJ
E4nujn5COska9tcDt2ILQuPA+48GQIoEnewOTHhGmSH1b8a281h57Lcb8sI9gN6m
14h/WDrPBgtioDQ+2Mhuqpc5rIXU7r4fTko/V1bGPaxCJL1901sfmGl8x2o6sTM3
zWvenbhGrjAe9hd0o8h9M+FZqJ+dJvWB4TB06mWn79MkzalYW2Tyy1/YJyKBRkqE
ZiPSs9yUrGK6Rz0ykpgSutesxQ+m+clFYf3aXgSDeBVflSXVA1ZG6E2SeT40k1SU
qcPSwv2TOuPUPLuWR2lJhSw5Dt1xa33ApQ67ZMEMAeFRILTxNfrl9EMiLQu8aGry
1JfssMYIAh5UHHneMwYbfh/nhv2jIv1WDGphFBCB5cVsh3WFIlNLr6VGQTvxPAuN
SQxZjNLiWIVR8cjAIzWcbXs5gM77sHE3480MWDOZM1YKN+JjcE4t36T20QkwJ8di
1HO0HYQLH6gxxjVscXjuYVi39k34N395eu9eEjOczWm7eDyv2iJJS7mDWniOE3ma
zEJ9Vp2hQXU6cxyowmp0aRMeHc4wS0mwJsZ5No2dVKK6NjGhl+WPQTZrRnJDcJX5
QLQkiAXmcAuEJxCwKM0v
=rDOW
-----END PGP SIGNATURE-----
