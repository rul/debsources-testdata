-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA256

Format: 3.0 (native)
Source: flashplugin-nonfree
Binary: flashplugin-nonfree
Architecture: i386 amd64
Version: 1:3.4
Maintainer: Bart Martens <bartm@debian.org>
Homepage: http://wiki.debian.org/FlashPlayer
Standards-Version: 3.9.4
Build-Depends: debhelper (>= 9)
Package-List: 
 flashplugin-nonfree deb contrib/web optional
Checksums-Sha1: 
 d92af5c5bb8341894942bacfba9da21f1418a59d 21080 flashplugin-nonfree_3.4.tar.gz
Checksums-Sha256: 
 9158ef4ee810b4e05ee0da68fb69f97d4b2cfe2f0ec647bcd0971e510e8a86a0 21080 flashplugin-nonfree_3.4.tar.gz
Files: 
 77a75d813839ff0e4897017d2b24d797 21080 flashplugin-nonfree_3.4.tar.gz

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.12 (GNU/Linux)

iQIcBAEBCAAGBQJSpNVAAAoJEDNV9NY7WCHMQeYQAJxyx3ZZ/ce6IiQhdLNtogXl
VqscF3R96pk37yGHagzf+OD/33cTLyFC+pZeVvu5lmzGfe33oKpcWsPQr6TjbSeS
twXr0kcsR5yqkTDmgC/c49XxoW+UBOSz1TeAHbH5/tKTJ/pfFQaRhtHi2i8ZlO5z
xojS4i60MP3mmtrxOAYCes933+WRq+14tOkayeUoPbUXdOUF824+msyx5dOWJKHJ
5Yj7rMFbOH5pWURnv0KizRIh1TUnrHYbYM46ap8YvC0Z7vhFZAZ0ffRyDufjLx1U
gQ7RLREqZPwxwNErn7ZOoJkXsMDPfQ9XOSet4KMcwGHMePnDxovggADmoYuTu03l
wKzaHeHhc5VOoNLU6ipBBhe+ZVYra5suZIJOqwbLzZ/uZVTTPHStv632vEEpN1xL
6BawhV6z3dincKuB1lO4G3ft4bTRH2hBTS/SOMK9RezZpVQ5OWhMYmr/vG3kueOJ
XVSkbmvWSxme/r2PYQ3ndCxUy4epUL+hnAlz/h+Nm2Yh+ZKLuW2iZ+tLWKAlOggH
eEWqbaG0ropCcm4B2c0U2zsJyEEBCdjksd9OkbVr2WTFKukhXFv+z8NGu6W9Z1fD
iECNRLx76WizeTED16WTb3qfgEE6j+6N2CsMsI8IZsTAy8AuDuyuRivPWNMRZR1Y
SwVCjg13B2n6Kwtcx2Gv
=C221
-----END PGP SIGNATURE-----
