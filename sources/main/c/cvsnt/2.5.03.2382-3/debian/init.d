#! /bin/sh
#
# cvslockserver
#
# Written by    Andreas Tscharner <andy@vis.ethz.ch>
#
# Modified skeleton script and took a look on the script
# on http://www.cvsnt.org/wiki/DebianLockdScript
#

### BEGIN INIT INFO
# Provides:          cvslockd
# Required-Start:    $network
# Required-Stop:     $network
# Should-Start:      $network
# Should-Stop:       $network
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Start the cvsnt lock server
# Description:       Lock server for cvsnt, replaces old file based
#                    directory locking mechanism
### END INIT INFO

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
DAEMON=/usr/bin/cvslockd
DNAME=cvslockd
NAME=cvsnt
DESC="CVSNT Lock Server Daemon"

test -x $DAEMON || exit 0

# Include cvsnt defaults if available
if [ -f /etc/default/$NAME ] ; then
	. /etc/default/$NAME
fi

set -e

case "$1" in
  start)
	echo -n "Starting $DESC: "
	start-stop-daemon --start --quiet --exec $DAEMON -- $DAEMON_OPTS
	echo "$DNAME."
	;;
  stop)
	echo -n "Stopping $DESC: "
	start-stop-daemon --stop --quiet --name $DNAME
	echo "$DNAME."
	;;
  restart|force-reload)
	echo -n "Restarting $DESC: "
	start-stop-daemon --stop --quiet --name $DNAME
	sleep 1
	start-stop-daemon --start --quiet --exec $DAEMON -- $DAEMON_OPTS
	echo "$DNAME."
	;;
  status)
	exit 4
	;;
  *)
	N=/etc/init.d/$NAME
	echo "Usage: $N {start|stop|restart|force-reload|status}" >&2
	exit 1
	;;
esac

exit 0
