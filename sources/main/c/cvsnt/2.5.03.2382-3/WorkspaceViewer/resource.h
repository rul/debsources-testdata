//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by WorkspaceViewer.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_WorkspaceManageTYPE         129
#define IDI_NETHOOD                     130
#define IDI_SERVER                      131
#define IDI_FLDRCLOSED                  132
#define IDI_FLDROPEN                    133
#define IDI_MACHINE                     134
#define IDI_DRIVE                       135
#define IDI_DOCUMENT                    136
#define IDR_LEFTMENU                    138
#define IDI_BRANCH                      139
#define IDI_ICON1                       146
#define IDI_WORLD                       146
#define IDD_DIALOG3                     147
#define IDC_NAME                        1001
#define IDC_TAG                         1002
#define IDC_ROOT                        1003
#define IDC_EDIT1                       1005
#define IDC_EDIT2                       1006
#define IDC_USERNAME                    1009
#define IDC_PASSWORD                    1010
#define IDC_CHECK1                      1011
#define IDC_WMVERSION                   1012
#define IDC_LIST1                       1013
#define IDC_CHECK2                      1016
#define IDC_CHECK3                      1017
#define IDC_CHECK4                      1018
#define IDC_CHECK5                      1019
#define IDC_CHECK6                      1020
#define IDC_MARCHHARE                   1023
#define IDC_MOREINFO                    1024
#define IDC_BUYNOW                      1025
#define ID_MENU                         32772
#define ID_PROPERTIES                   32774
#define ID_ANNOTATE                     32775
#define ID_VIEW                         32776
#define ID_MENU_CHECKOUT                32777
#define ID_MENU_VIEW                    32778
#define ID_MENU_ANNOTATE                32779
#define ID_MENU_PROPERTIES              32780
#define ID_VIEW_FILES                   32781
#define ID_VIEW_TAGS                    32782
#define ID_VIEW_BRANCHES                32783
#define ID_NEW                          32785
#define ID_MENU_DELETE                  32787
#define ID_MENU_LOG                     32790
#define ID_VIEW_REVISIONS               32791
#define ID_VIEW_SHOWGLOBALSERVERS       32792
#define ID_MENU_PERMISSIONS             32793
#define ID_FILE_VIEW                    32796

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        148
#define _APS_NEXT_COMMAND_VALUE         32800
#define _APS_NEXT_CONTROL_VALUE         1026
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
