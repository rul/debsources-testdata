#!/usr/bin/make -f

# Enable compiler hardening flags.  Do not enable PIE; it interferes with
# some of the things that gnubg does to probe for supported CPU
# instructions.
export DEB_BUILD_MAINT_OPTIONS = hardening=+bindnow

# Add --as-needed to drop a few unnecessary dependencies.
export DEB_LDFLAGS_MAINT_APPEND = -Wl,--as-needed

# Only build with SSE on amd64.  We can't assume that processor for i386,
# and it fails to build on ia64 since GCC there doesn't support -msse or
# -msse2 (as of 2010-01-17).
DEB_HOST_ARCH_CPU ?= $(shell dpkg-architecture -qDEB_HOST_ARCH_CPU)
ifeq ($(DEB_HOST_ARCH_CPU),amd64)
    SSE = --enable-simd=sse2
else
    SSE = --enable-simd=no
endif

%:
	dh $@ --with autoreconf

override_dh_autoreconf:
	dh_autoreconf --as-needed

override_dh_auto_configure:
	dh_auto_configure -- --with-gtk --with-board3d --with-sqlite \
	    --with-python --enable-threads $(SSE)

# Disable building the rollout databases during an architecture-specific
# build.  It takes too long and is too large.  We'll instead build the
# one-sided database only during an arch-independent build and buid the
# two-sided database during installation.
override_dh_auto_build-arch:
	echo 'stop' > gnubg_ts0.bd
	echo 'stop' > gnubg_os0.bd
	dh_auto_build

override_dh_auto_build-indep:
	echo 'stop' > gnubg_ts0.bd
	rm -f gnubg_os0.bd
	dh_auto_build

override_dh_auto_install:
	$(MAKE) install DESTDIR=$(CURDIR)/debian/gnubg
	rm debian/gnubg/usr/share/gnubg/gnubg_ts0.bd
	rm -r debian/gnubg/usr/share/gnubg/fonts
	perl -i -pe 's/^(gnubg\s+)-(bcd\S+)/$$1\\fB\\-$$2\\fR/' \
	    debian/gnubg/usr/share/man/man6/gnubg.6
	mv debian/gnubg/usr/bin debian/gnubg/usr/games
	install -d debian/gnubg/usr/share/applications
	install -m 644 debian/gnubg.desktop \
	    debian/gnubg/usr/share/applications/gnubg.desktop
	install -d debian/gnubg/var/lib/gnubg
	install -d debian/gnubg/usr/lib/gnubg
	install -d debian/gnubg-data/usr/share
	mv debian/gnubg/usr/share/gnubg debian/gnubg-data/usr/share/
	mv debian/gnubg-data/usr/share/gnubg/gnubg.wd \
	    debian/gnubg/usr/lib/gnubg/gnubg.wd
	install -d debian/gnubg/usr/share/gnubg
	ln -s /usr/lib/gnubg/gnubg.wd \
	    debian/gnubg/usr/share/gnubg/gnubg.wd
	mv debian/gnubg/usr/share/locale debian/gnubg-data/usr/share/
	install -d debian/gnubg-data/usr/share/gnubg/fonts
	ln -s ../../fonts/truetype/ttf-dejavu/DejaVuSans.ttf \
	    debian/gnubg-data/usr/share/gnubg/fonts/Vera.ttf
	ln -s ../../fonts/truetype/ttf-dejavu/DejaVuSans-Bold.ttf \
	    debian/gnubg-data/usr/share/gnubg/fonts/VeraBd.ttf
	ln -s ../../fonts/truetype/ttf-dejavu/DejaVuSerif-Bold.ttf \
	    debian/gnubg-data/usr/share/gnubg/fonts/VeraSeBd.ttf
	install -d debian/gnubg-data/usr/share/doc/gnubg
	mv debian/gnubg/usr/share/doc/gnubg/images \
	    debian/gnubg-data/usr/share/doc/gnubg/
	mv debian/gnubg/usr/share/doc/gnubg/*.html \
	    debian/gnubg-data/usr/share/doc/gnubg/
	install -m 644 doc/*.pdf debian/gnubg-data/usr/share/doc/gnubg/
	install -d debian/gnubg/usr/share/doc/gnubg/examples
	mv debian/gnubg-data/usr/share/gnubg/scripts/query_player.sh \
	    debian/gnubg/usr/share/doc/gnubg/examples/

override_dh_installchangelogs:
	dh_installchangelogs -i ChangeLog
	dh_installchangelogs -a -XChangeLog
	[ ! -f debian/gnubg-data/usr/share/doc/gnubg-data/changelog ] \
	    || mv debian/gnubg-data/usr/share/doc/gnubg-data/changelog \
		debian/gnubg-data/usr/share/doc/gnubg/changelog

# Do not compress the PDF documentation.
override_dh_compress:
	dh_compress -X.pdf

override_dh_builddeb:
	dh_builddeb -- -Zxz
