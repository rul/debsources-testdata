.\" Copyright (c) 2003-2009, John Wiegley.  All rights reserved.
.\" 
.\" Redistribution and use in source and binary forms, with or without
.\" modification, are permitted provided that the following conditions are
.\" met:
.\" 
.\" - Redistributions of source code must retain the above copyright
.\"   notice, this list of conditions and the following disclaimer.
.\" 
.\" - Redistributions in binary form must reproduce the above copyright
.\"   notice, this list of conditions and the following disclaimer in the
.\"   documentation and/or other materials provided with the distribution.
.\" 
.\" - Neither the name of New Artisans LLC nor the names of its
.\"   contributors may be used to endorse or promote products derived from
.\"   this software without specific prior written permission.
.\" 
.\" THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
.\" "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
.\" LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
.\" A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
.\" OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
.\" SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
.\" LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
.\" DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
.\" THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
.\" (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
.\" OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
.TH LEDGER 1 2009-08-06 "Ledger 2.6.2"
.SH NAME
ledger \- command-line accounting
.SH SYNOPSIS
.B ledger
.RI [ options ] " command " [ acct-regex "]... [\-\- " payee-regex ...]
.SH DESCRIPTION
Ledger is a command-line accounting tool with the moxie to exist.
It provides no bells or whistles, and returns the user to the days
before user interfaces were even a twinkling in their father's CRT.
.SH NOTE
This man page is a summary of the command-line usage of
.B ledger
along with a short example of a Ledger data file.
For more complete documentation see the Ledger Reference Manual, (via
the
.B info ledger
command or otherwise).
.SH COMMANDS
All commands require a Ledger data file which can be specified with
.BI -f "\ filename"
or via the
.B LEDGER_FILE
environment variable.

The
.BR balance " and " register
commands provide the primary functionality of Ledger and are used most
frequently:
.RS 4
.TP 4
.BR bal ", " balance " [REGEXP]..."
Reports the current balance of all matching accounts.
If an account contains multiple types of commodities, each
commodity's total is reported separately.
.TP
.BR reg ", " register " [REGEXP]..."
Displays all the transactions occurring in the matching accounts, line
by line.
The output from
.B register
is very close to what a typical checkbook, or single-account ledger,
would look like.  It also shows a running balance.  The final running
balance of any register should always be the same as the current
balance of that account.
.RE

Several commands are effectively variants of
.BR register .
These commands accept the same options and display the same
transactions as
.B register
and differ only in the format of the output:
.RS 4
.TP 4
.BR print " [REGEXP]..."
Displays transactions in a format that can be parsed by Ledger.
They will be properly formatted and output in the most economic form
possible.
The
.B print
command can be a handy way to clean up a Ledger file whose formatting
has gotten out of hand.
.TP
.BR xml " [REGEXP]..."
Displays transactions in an XML format that can then be read and
processed by external tools.
Use the
.B \-\-totals
option to include the running total with each transaction.
.TP
.BR emacs " [REGEXP]..."
Displays transactions in a format that can be read directly by Emacs
Lisp.
.RE

The remaining commands are each useful in particular circumstances:
.RS 4
.TP 4
.BR equity " [REGEXP]..."
Prints out account balances as if they were entries.
This makes it easy to establish the starting balances for accounts,
(such as when beginning a new Ledger file to then archive previous
years).
.TP
.BR prices " [REGEXP]..."
Displays the price history for matching commodities.
The
.B \-A
option is useful with this report to display the average running
price, or
.B \-D
to show each price's deviation from that average.
.TP
.BR pricedb " [REGEXP]..."
Produces the same information as
.B prices
but in a format that can be parsed by Ledger.
.TP
.BI entry " DATE PAYEE AMOUNT"
Output a derived entry, based on the arguments and an account matching
.I PAYEE
in the transacation history. If Ledger does not succeed in generating
a new entry, an error is printed and the exit code is set to 1.
.RE
.SH OPTIONS
\ 
.SS Basic options
.TP
.B \-h, \-\-help
Print a summary of the basic options and commands.
This help screen is also printed if
.B ledger
is run without a command.
.TP
.B \-H, \-\-full-help
Print a help message including all command-line options.
.TP
.B \-v, \-\-version
Prints the current version of Ledger and exits.
This is useful for sending bug reports, to let the author know which
version of Ledger you are using.
.TP
.BI \-f,\ \-\-file \ FILE
Reads
.I FILE
as a Ledger file.
Typically, the environment variable
.B LEDGER_FILE
is set rather than using this command-line option.
.TP
.BI \-o,\ \-\-output \ FILE
Redirects output from any command to
.IR FILE \.
By default, all output goes to standard output.
.TP
.BI \-i,\ \-\-init\-file\  FILE
Causes
.I FILE
to be read by
.B ledger
before any other Ledger file.
This file may not contain any transactions, but it may contain option
settings.
To specify options in the init file, use the same syntax as the
command-line, but put each option on it's own line.
Option settings on the command-line or in the environment always take
precedence over settings in the init file.
The default init file is
.BR ~/.ledgerrc \.
.TP
.BI \-\-cache\  FILE
Identifies
.I FILE
as the default binary cache file.
That is, whenever a command is finished a binary copy of the input
files will be written to the specified cache, to speed up the loading
time of subsequent queries.
This filename can also be given using the environment variable
.B LEDGER_CACHE
or by putting the option into your init file.
.TP
.B \-\-no\-cache
Causes Ledger to always ignore the binary cache.
.TP
.BI \-a,\ \-\-account\  NAME
Specifies the default account which QIF file transactions are assumed
to relate to.
.SS Report filtering options
These options change which transactions affect the outcome of a
report, in ways other than just using regular expressions:
.TP
.B \-c,\  \-\-current
Displays only entries occurring on or before the current date.
.TP
.BI \-b,\ \-\-begin\  DATE
Constrains the report to entries on or after
.IR DATE \.
Only entries after that date will be calculated, which means that the
running total in the balance report will always start at zero with the
first matching entry.
Note: This is different from using
.B \-\-display
to constrain what is displayed.
.TP
.BI \-e,\ \-\-end\  DATE
Constrains the report so that entries on or after
.I DATE
are not considered.
The ending date is inclusive.
.TP
.BI \-p,\ \-\-period\  STR
Sets the reporting period to
.IR STR \.
This will subtotal all matching entries within each period separately,
making it easy to see
.BR weekly ,\  monthly ,\  quarterly ,
etc. transaction totals.
A period string can even specify the beginning and end of the report
range, using simple terms like
.B last june
or
.BR "next month" \.
For more using period expressions, see the Period Expressions section
of the Ledger Reference Manual.
.TP
.BI \-\-period\-sort\  EXPR
Sorts the transactions within each reporting period using the value
expression
.IR EXPR \.
This is most often useful when reporting monthly expenses.
For example, to view the highest expense categories at the top of each
month use:

.B "ledger -M --period-sort -At reg ^Expenses"
.TP
.B \-C, \-\-cleared
Displays only transactions whose entry has been marked cleared (by
placing an asterix to the right of the date).
.TP
.B \-U, \-\-uncleared
Displays only transactions whose entry has not been marked cleared
(i.e., if there is no asterix to the right of the date).
.TP
.B \-R, \-\-real
Displays only real transactions, not virtual.
A virtual transaction is indicated by surrounding the account name
with parentheses or brackets, (see the Ledger Reference Manual for
more on using virtual transactions).
.TP
.B \-L, \-\-actual
Displays only actual transactions, and not those created due to
automated transactions.
.TP
.B \-r, \-\-related
Displays transactions that are related to whichever transactions would
otherwise have matched the filtering criteria.
In the register report, this shows where money went to, or the account
it came from.
In the balance report, it shows all the accounts affected by entries
having a related transaction.
.TP
.B \-\-budget
Display budgeted transactions only.
.TP
.B \-\-add\-budget
Shows both budgeted and unbudgeted transactions.
This option is useful for displaying how close your actual
transactions meet your budget.
.TP
.B \-\-unbudgeted
Show only unbudgeted transactions.
.TP
.BI \-\-forecast\  EXPR
Projects your budget into the future, (while
.I EXPR
is true), showing how it will affect future balances.
.TP
.BI \-l,\ \-\-limit\  EXPR
Calculate only transactions matching
.IR EXPR \.
(See the Value Expressions section of Ledger Reference Manual for more
details on possible values of
.I EXPR
for this and other options.)
.TP
.BI \-t,\ \-\-amount\  EXPR
Sets the value expression used to calculate the value column in the
.B register
report, the account totals in the
.B balance
report, and the values printed in the
.B equity
report.
.TP
.BI \-T,\ \-\-total\  EXPR
Sets the value expression used for the
.B totals
column in the
.BR register " and " balance
reports.
.I EXPR
to calculate the displayed total.
.SS Output customization options
.TP
.B \-n,\ \-\-collapse
Causes entries in a
.B register
report with multiple transactions to be collapsed into a single,
subtotaled entry.
.TP
.B \-s,\ \-\-subtotal
Causes all entries in a
.B register
report to be collapsed into a single, subtotaled entry.
.TP
.B \-P,\ \-\-by\-payee
Reports subtotals by payee.
.TP
.B \-x,\ \-\-comm\-as\-payee
Changes the payee of every transaction to be the commodity used in
that transaction.
This can be useful when combined with other options, such as
.BR \-s,\ \-\-sort \.
.TP
.B \-E,\ \-\-empty
Includes even empty accounts in the
.B balance
report.
.TP
.B \-W,\ \-\-weekly
reports transaction totals by the week.
The week begins on whichever day of the week begins the month
containing that transaction.  To set a specific begin date, use a
period string, such as
.B \-\-period weekly from
.IR DATE \.
.TP
.B \-M,\ \-\-monthly
Reports transaction totals by month.
.TP
.B \-Y,\ \-\-yearly
Reports transaction totals by year.
.TP
.B \-\-dow
Reports transactions totals for each day of the week.
This is an easy way to see if weekend spending is more than on
weekdays.
.TP
.B \-S,\ \-\-sort EXPR
Sorts a report by comparing the values determined using the value
expression
.IR EXPR \.
For example, using
.B \-\-sort date
will sort by date, (useful if included files cover different date
ranges), and
.B \-\-sort \-UT
in the balance report will sort account balances from greatest to
least, using the absolute value of the total.
For more on how to use value expressions, see the Value Expressions
section of the Ledger Reference Manual.
.TP
.B \-w,\ \-\-wide
Causes the default
.B register
report to assume 132 columns instead of 80.
.TP
.BI \-\-head\  COUNT
Show only the first
.I COUNT
entries.
If a negative amount is given, it will invert the meaning of the flag
(instead of the first five entries being printed, for example, it
would print all but the first five).
.TP
.BI \-\-tail\  COUNT
Show only the last
.I COUNT
entries.
If a negative amount is given, it will invert the meaning of the flag
(instead of the last five entries being printed, for example, it
would print all but the last five).
.TP
.B \-\-pager PAGER
Tells
.B ledger
to pass its output to the given pager program---very useful when the
output is especially long.
This behavior can be made the default by setting the
.B LEDGER_PAGER
environment variable.
.TP
.B \-A,\ \-\-average
Reports the average transaction value.
.TP
.B \-D,\ \-\-deviation
Reports each transaction's deviation from the average.
It is only meaningful in the
.BR register " and " prices
reports.
.TP
.B \-%,\ \-\-percentage
Shows account subtotals in the
.B balance
report as percentages of the parent account.
.TP
.B \-\-totals
Include running total information in the
.B xml
report.
.TP
.B \-j,\ \-\-amount\-data
Changes the
.B register
report so that it outputs nothing but the date and the value column,
and the latter without commodities.
This is only meaningful if the report uses a single commodity.
This data can then be fed to other programs, which could plot the
date, analyze it, etc.
.TP
.B \-J,\ \-\-total\-data
Changes the
.B register
report so that it outputs nothing but the date and totals column,
without commodities.
.TP
.BI \-d,\ \-\-display\  EXPR
Limits which transactions or accounts or actually displayed in a
report.
They might still be calculated, and be part of the running total of a
register report, for example, but they will not be displayed.
.TP
.BI \-y,\ \-\-date\-format\  STR
Changes the basic date format used by reports.
The default uses a date like 2004/08/01, which represents the default
date format of
.BR %Y/%m/%d \.
To change the way dates are printed in general, the easiest way is to
put
.BI \-\-date\-format\  FORMAT
in the Ledger initialization file
.B ~/.ledgerrc
(or the file referred to by
.BR LEDGER_INIT ).
.TP
.BI \-F,\ \-\-format\  STR
Sets the reporting format for whatever report ledger is about to make.
See the Format Strings section of the Ledger Reference Manual for details.
.SS Commodity price options
These options affect how commodity values are displayed:
.TP
.BI \-\-price\-db\  FILE
Sets the file that is used for recording downloaded commodity prices.
It is always read on startup, to determine historical prices.
The default file is
.BR ~/.pricedb \.
.TP
.BI \-L,\ \-\-price\-exp\  MINS
Sets the expected freshness of price quotes, in minutes.
That is, if the last known quote for any commodity is older than this
value---and if
.B \-\-download
is being used---then the internet will be consulted again for a newer
price.
Otherwise, the old price is still considered to be fresh enough.
(Default value is 1440 minutes.)
.TP
.B \-Q,\ \-\-download
Causes quotes to be automatically downloaded, as needed, by running a
script named
.B getquote
and expecting that script to return a value understood by ledger.
A sample implementation of a
.B getquote
script, implemented in Perl, is provided in the distribution.
Downloaded quote price are then appended to the price database,
usually specified using the environment variable
.BR LEDGER_PRICE_DB \.
.SS Commodity reporting options
There are several different ways that ledger can report the totals it
displays.
The most flexible way to adjust them is by using value expressions
and the
.BR \-t " and " \-T
options.
However, there are also several standard reports, which will satisfy
most users' basic reporting needs:
.TP
.B \-O,\ \-\-quantity
Reports commodity totals (this is the default).
.TP
.B \-B,\ \-\-basis
Reports the cost basis for all transactions.
.TP
.B \-V,\ \-\-market
Reports the last known market value for all commodities.
.TP
.B \-g,\ \-\-performance
Reports the net gain/loss for each transaction in a
.B register
report.
.TP
.B \-G,\ \-\-gain
Reports the net gain/loss for all commodities in the report that have
a price history.
.SH ENVIRONMENT
Every option to ledger may be set using an environment variable.  If
an option has a long name such as
.B \-\-this\-option
then setting the
environment variable
.B LEDGER_THIS_OPTION
will have the same effect as specifying that option on the
command-line.
Options on the command-line always take precedence over environment
variable settings, however.
Note that you may also permanently specify option values by placing
option settings in the file
.B ~/.ledgerrc
by default, (or the file specified by the
.B LEDGER_INIT_FILE
environment variable).

Of special note is the
.B LEDGER_FILE
environment variable which almost all users of Ledger will find convenient:
.TP
.B LEDGER_FILE
Set to a file, to be read by the
.B ledger
command.
This avoids the requirement to pass
.BI \-\-file\  FILE
to every invocation of
.BR ledger \.
.SH EXAMPLE
Here is sample data file
.RB ( ledger.dat
from the distribution) demonstrating most of the features of the
ledger data-file format.
These include comments
.RB ( ; ),
automated transactions
.RB ( = ),
virtual transactions (
.BI ( "account-name" )
), periodic (budget) transactions
.RB ( ~ ),
cleared transactions
.RB ( * ),
commodity transactions
.RI ( SYMBOL
.BR @ ),
and check numbers (
.BI ( NUMBER )
).
.in +4n
.nf

; Sample file ledger.dat

; An automated transaction to a virtual account
= /^Expenses:Books/
  (Liabilities:Taxes)             -0.10

; A periodic (budget) transaction
~ Monthly
  Assets:Bank:Checking          $500.00
  Income:Salary

; A cleared transaction
2004/05/01 * Checking balance
  Assets:Bank:Checking        $1,000.00
  Equity:Opening Balances

; A transaction involving multiple commodities
2004/05/01 * Investment balance
  Assets:Brokerage              50 AAPL @ $30.00
  Equity:Opening Balances

2004/05/14 * Pay day
  Assets:Bank:Checking          $500.00
  Income:Salary

2004/05/27 Book Store
  Expenses:Books                 $20.00
  Liabilities:MasterCard

; A transaction with a check number
2004/05/27 (100) Credit card company
  Liabilities:MasterCard         $20.00
  Assets:Bank:Checking

.fi
.in
.SH SEE ALSO
The Ledger Reference Manual available via
.B info ledger
if
.BR ledger " and " info
are properly installed.

The ledger homepage:
.B http://wiki.github.com/jwiegley/ledger
