/* @(#)serrmsg.c	1.2 01/11/09 Copyright 1985,2000 J. Schilling */
/*
 *	Routines for printing command errors
 *
 *	Copyright (c) 1985,2000 J. Schilling
 */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, write to
 * the Free Software Foundation, 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */

#include <mconfig.h>
#include <unixstd.h>		/* include <sys/types.h> try to get size_t */
#include <stdio.h>		/* Try again for size_t	*/
#include <stdxlib.h>		/* Try again for size_t	*/
#include <standard.h>
#include <stdxlib.h>
#include <vadefs.h>
#include <strdefs.h>
#include <schily.h>
#include <errno.h>

EXPORT	int	serrmsg		__PR((char *buf, size_t maxcnt, const char *, ...));
EXPORT	int	serrmsgno	__PR((int, char *buf, size_t maxcnt, const char *, ...));
LOCAL	int	_serrmsg	__PR((int, char *buf, size_t maxcnt, const char *, va_list));

/* VARARGS1 */
EXPORT int
serrmsg(char *buf, size_t maxcnt, const char *msg, ...)
{
	va_list	args;
	int	ret;

	va_start(args, msg);
	ret = _serrmsg(errno, buf, maxcnt, msg, args);
	va_end(args);
	return (ret);
}

/* VARARGS2 */
EXPORT int
serrmsgno(int err, char *buf, size_t maxcnt, const char *msg, ...)
{
	va_list	args;
	int	ret;

	va_start(args, msg);
	ret = _serrmsg(err, buf, maxcnt, msg, args);
	va_end(args);
	return (ret);
}

LOCAL int
_serrmsg(int err, char *buf, size_t maxcnt, const char *msg, va_list args)
{
	int	ret;
	char	errbuf[20];
	char	*errnam;
	char	*prognam = get_progname();
	
	if (err < 0) {
		ret = js_snprintf(buf, maxcnt, "%s: %r", prognam, msg, args);
	} else {
		errnam = strerror(err);
		if (errnam == NULL) {
			(void)snprintf(errbuf, sizeof (errbuf),
						"Error %d", err);
			errnam = errbuf;
		}
		ret = js_snprintf(buf, maxcnt,
				"%s: %s. %r", prognam, errnam, msg, args);
	}
	return (ret);
}
