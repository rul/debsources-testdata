#include "../xconfig.h"

#include "compat.h"

static const char RCSID[]="$Id: compat.c,v 1.1.1.1 2003/12/11 23:22:08 bero Exp $";

#ifndef HAVE_STRSEP
/* Match STRING against the filename pattern PATTERN, returning zero if
   it matches, nonzero if not.  */
char *strsep(char **stringp, const char *delim)
{
    char *res;
    
    if(!stringp || !*stringp || !**stringp)
        return (char*)0;
    
    res = *stringp;
    while(**stringp && !strchr(delim,**stringp))
        (*stringp)++;
    
    if(**stringp) {
        **stringp = '\0';
        (*stringp)++;
    }
    
    return res;
}
#endif  /* HAVE_STRSEP */
