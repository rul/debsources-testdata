/* @(#)btorder.h	1.15 01/12/09 Copyright 1996 J. Schilling */
/*
 *	Definitions for Bit and Byte ordering
 *
 *	Copyright (c) 1996 J. Schilling
 */
/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2, or (at your option)
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; see the file COPYING.  If not, write to
 * the Free Software Foundation, 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */


#ifndef	_BTORDER_H
#define	_BTORDER_H

#ifndef	_INCL_SYS_TYPES_H
#include <sys/types.h>			/* try to load isa_defs.h on Solaris */
#define	_INCL_SYS_TYPES_H
#endif

#ifndef _MCONFIG_H
#include <mconfig.h>			/* load bit/byte-oder from xmconfig.h*/
#endif

#include <xconfig.h>

/*
 * Convert bit-order definitions from xconfig.h into legacy values
 */
#ifdef BITFIELDS_HTOL
#define _BIT_FIELDS_HTOL
#else
#define BITFIELDS_LTOH
#define _BIT_FIELDS_LTOH
#endif

/*
 * Convert byte-order definitions from xconfig.h into legacy values.
 * Note that we cannot use the definitions _LITTLE_ENDIAN and _BIG_ENDIAN
 * because they are used on IRIX-6.5 with different meaning.
 */
#if !defined(WORDS_BIGENDIAN)
#define	WORDS_LITTLEENDIAN
/*#define	_LITTLE_ENDIAN*/
#endif

#if defined(WORDS_BIGENDIAN)
#undef	WORDS_LITTLEENDIAN
/*#define	_BIG_ENDIAN*/
#endif
#endif	/* _BTORDER_H */
