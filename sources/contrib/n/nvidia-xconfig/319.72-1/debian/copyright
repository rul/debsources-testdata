Format: http://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: nvidia-xconfig
Upstream-Contact: NVIDIA Corporation
Source: ftp://download.nvidia.com/XFree86/nvidia-xconfig/

Files: *
Copyright: (C) 2004-2012 NVIDIA Corporation
License: GPL-2

Files: nvidia-cfg.h
Copyright: (c) 2004  NVIDIA Corp.  All rights reserved.
License: other
 NOTICE TO USER:   The source code  is copyrighted under  U.S. and
 international laws. NVIDIA, Corp. of Santa Clara, California owns
 the copyright  and as design patents  pending  on the design  and
 interface  of the NV chips.   Users and possessors of this source
 code are hereby granted  a nonexclusive,  royalty-free  copyright
 and  design  patent license  to use this code  in individual  and
 commercial software.
 .
 Any use of this source code must include,  in the user documenta-
 tion and  internal comments to the code,  notices to the end user
 as follows:
 .
 Copyright (c) 2004 NVIDIA Corp.  NVIDIA design patents pending in
 the U.S. and foreign countries.
 .
 NVIDIA CORP.  MAKES  NO REPRESENTATION  ABOUT  THE SUITABILITY OF
 THIS SOURCE CODE FOR ANY PURPOSE.  IT IS PROVIDED "AS IS" WITHOUT
 EXPRESS OR IMPLIED WARRANTY OF ANY KIND.  NVIDIA, CORP. DISCLAIMS
 ALL WARRANTIES  WITH REGARD  TO THIS SOURCE CODE,  INCLUDING  ALL
 IMPLIED   WARRANTIES  OF  MERCHANTABILITY  AND   FITNESS   FOR  A
 PARTICULAR  PURPOSE.   IN NO EVENT SHALL NVIDIA, CORP.  BE LIABLE
 FOR ANY SPECIAL, INDIRECT, INCIDENTAL,  OR CONSEQUENTIAL DAMAGES,
 OR ANY DAMAGES  WHATSOEVER  RESULTING  FROM LOSS OF USE,  DATA OR
 PROFITS,  WHETHER IN AN ACTION  OF CONTRACT,  NEGLIGENCE OR OTHER
 TORTIOUS ACTION, ARISING OUT  OF OR IN CONNECTION WITH THE USE OR
 PERFORMANCE OF THIS SOURCE CODE.
 .
 As clarified by experience with other pakcages, NVIDIA applies a very
 generous interpretation of the term "use" in this license:
 .
 GPL licensing note -- nVidia is allowing a liberal interpretation of the
 documentation restriction above, to merely say that this nVidia's
 copyright and disclaimer should be included with all code derived from
 this source.

Files: XF86Config-parser/*
Copyright: (c) 1997  Metro Link Incorporated
           (c) 1997-2003 by The XFree86 Project, Inc.
License: other-MetroLink and other-XFree

Files: XF86Config-parser/DRI.c
Copyright: 1999 Precision Insight, Inc., Cedar Park, Texas.  All Rights Reserved.
License: Expat-Precision
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice (including the next
 paragraph) shall be included in all copies or substantial portions of the
 Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 PRECISION INSIGHT AND/OR ITS SUPPLIERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

Files: XF86Config-parser/Extensions.c
Copyright: 2004 Red Hat Inc., Raleigh, North Carolina.  All Rights Reserved.
License: Expat-RedHat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation on the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software,
 and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 .
 The above copyright notice and this permission notice (including the
 next paragraph) shall be included in all copies or substantial
 portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NON-INFRINGEMENT.  IN NO EVENT SHALL RED HAT AND/OR THEIR SUPPLIERS
 BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

Files: XF86Config-parser/Generate.c
Copyright: (C) 2005 NVIDIA Corporation
           1999-2002 Red Hat, Inc.
           2002 Red Hat, Inc.
License: GPL-2+ and other-GPL
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 2 of the License, or (at your
 option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this package; if not, write to the Free Software Foundation, Inc.,
 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public License
 version 2 can be found in the file /usr/share/common-licenses/GPL-2.
 .
 This file is also derived from work covered by the following license:
 .
 This software may be freely redistributed under the terms of the GNU
 library public license.
 .
 For the purposes of this distribution, we invoke the terms of the GNU
 Library General Public License allowing work under that license to be
 distributed under the terms of the GNU General Public License instead.

Files: XF86Config-parser/Merge.c
Copyright: (c) 1997  Metro Link Incorporated
License: other-MetroLink

Files: XF86Config-parser/Util.c
Copyright: (C) 2005 NVIDIA Corporation
License: GPL-2

Files: XF86Config-parser/configProcs.h
Copyright: (c) 1997-2001 by The XFree86 Project, Inc.
License: other-XFree

Files: debian/*
Copyright: © 2005 Randall Donald <rdonald@debian.org>
           © 2010-2013 Andreas Beckmann <anbe@debian.org>
License: GPL-2+

License: GPL-2
 This program is free software; you can redistribute it and/or modify it
 under the terms and conditions of the GNU General Public License,
 version 2, as published by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this package; if not, write to the Free Software Foundation, Inc.,
 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public License
 version 2 can be found in the file /usr/share/common-licenses/GPL-2.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify it
 under the terms of the GNU General Public License as published by the
 Free Software Foundation; either version 2 of the License, or (at your
 option) any later version.
 .
 This program is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
 Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this package; if not, write to the Free Software Foundation, Inc.,
 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public License
 version 2 can be found in the file /usr/share/common-licenses/GPL-2.

License: other-Metrolink
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 THE X CONSORTIUM BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
 OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 .
 Except as contained in this notice, the name of the Metro Link shall not be
 used in advertising or otherwise to promote the sale, use or other dealings
 in this Software without prior written authorization from Metro Link.

License: other-XFree
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 THE COPYRIGHT HOLDER(S) OR AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR
 OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 .
 Except as contained in this notice, the name of the copyright holder(s)
 and author(s) shall not be used in advertising or otherwise to promote
 the sale, use or other dealings in this Software without prior written
 authorization from the copyright holder(s) and author(s).
